function [dxdt] = simYASA(t,x,sysParam)

% x(1) = i_q
% x(2) = i_d
% x(3) = i_0
% x(4) = w_r
% x(5) = theta_r

%system parameters
Lls = sysParam.Lls; %henries
Lms = sysParam.Lms; %henries
lambda_m = sysParam.lambda_m; %V*s/rad
R = sysParam.R; %ohm
m = sysParam.m; %kg
r_out = sysParam.r_out; %m
r_in = sysParam.r_in; %m
J = sysParam.J; %kg*m^2
B_m = sysParam.B_m;
Pole_pairs = sysParam.Pole_pairs;
T_L = sysParam.T_L; %Nm

%compute theta in reference frame of the ROTOR
theta = x(5);

%compute balanced 3 phase voltages for all time
Batt_volt = sysParam.BattV; %Volts
V_peak = Batt_volt*sqrt(2);
v_as = V_peak*cos(x(5));
v_bs = V_peak*cos(x(5)-2/3*pi);
v_cs = V_peak*cos(x(5)+2/3*pi);

%transform to rotor ref frame that has ELECTRICAL angular velocity of
%rotor
[v_q,v_d,v_0] = abcs2qd0(theta,v_as,v_bs,v_cs);
V_phase.q = v_q;
V_phase.d = v_d;
V_phase.zero = v_0;

%torque eqn1
T_e = 3/2*Pole_pairs/2*lambda_m*x(1);

%torque equation using lorentz force method from literature--not used
% kd = r_in/r_out;
% B_max = 2; %Tesla
% m = 3; %number of phases
% N_ph = 500; %number of turns per phase
% A_in = m*N_ph*x(1)/(pi*r_in); %linear current density
% T_e = 2*pi*B_max*A_in*r_out^3*kd*(1-kd^2);

%perform step change
if(t<sysParam.StepInputTime)
    V_phase.q=0;
    V_phase.d=0;
    V_phase.zero=0;
    T_L = 0;
end

%state equations
dxdt(1) = (-R*x(1) - x(4)*(Lls+3/2*Lms)*x(2) - x(4)*lambda_m + V_phase.q)*(Lls+3/2*Lms)^-1;
dxdt(2) = (-R*x(2) + x(4)*(Lls+3/2*Lms)*x(1) + V_phase.d)*(Lls+3/2*Lms)^-1;
dxdt(3) = (-R*x(3) + V_phase.zero)*(Lls)^-1;
dxdt(4) = (T_e - B_m*2/Pole_pairs*x(4) - T_L)*(J*2/Pole_pairs)^-1;
dxdt(5) = x(4); %integrate omega to get total radians electrically turned by rotor

dxdt = dxdt';


end

