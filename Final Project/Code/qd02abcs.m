function [x_as,x_bs,x_cs] = qd02abcs(theta,x_q,x_d,x_0)

%make input matrix
x = [x_q; x_d; x_0];

%calculate Ks for each timestep and corresponding phase var value
for k = 1:length(theta)
    Ks{k} = 2/3*[cos(theta(k)) cos(theta(k)-2/3*pi) cos(theta(k)+2/3*pi);...
        sin(theta(k)) sin(theta(k)-2/3*pi) sin(theta(k)+2/3*pi);...
        1/2 1/2 1/2];

    x_abcs(:,k) = Ks{k}^-1*x(:,k);
    x_as(k) = x_abcs(1,k);
    x_bs(k) = x_abcs(2,k);
    x_cs(k) = x_abcs(3,k);
end

end

