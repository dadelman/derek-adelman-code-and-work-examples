function [x_q,x_d,x_0] = abcs2qd0(theta,x_as,x_bs,x_cs)

%make input matrix
x = [x_as; x_bs; x_cs];

%calculate Ks for each timestep and corresponding phase var value
for k = 1:length(theta)
    Ks{k} = 2/3*[cos(theta(k)) cos(theta(k)-2/3*pi) cos(theta(k)+2/3*pi);...
        sin(theta(k)) sin(theta(k)-2/3*pi) sin(theta(k)+2/3*pi);...
        1/2 1/2 1/2];
    
    x_qd0(:,k) = Ks{k}*x(:,k);
    x_q(k) = x_qd0(1,k);
    x_d(k) = x_qd0(2,k);
    x_0(k) = x_qd0(3,k);
end
end

