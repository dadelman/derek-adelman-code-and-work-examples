%% Derek Adelman ENGR 570 Final Project
%%
clear
clc
format compact
close all
%% Open Loop System Control

%system parameters
sysParam.Lls = 1e-3; %henries
sysParam.Lms = 10e-3; %henries
sysParam.lambda_m = .8; %V*s/rad
sysParam.R = 1; %ohm
sysParam.m = 5; %kg
sysParam.r_out = .294; %m
sysParam.r_in = .04; %m
sysParam.J = 1/2*sysParam.m*(sysParam.r_out^2-sysParam.r_in^2); %kg*m^2
sysParam.B_m = 0.0; %damping coefficient
sysParam.Pole_pairs = 4;
sysParam.T_L = 0; %Nm
sysParam.freq = 300; %rad/s--note not required in current revision of code (brushless dc operation)
sysParam.BattV = 340; %Volts
sysParam.StepInputTime = .1;%sec

%simulate axial flux motor
%ode45 setup
tspan = [0 10]; %simulationg start/end time in sec
x0 = [0 0 0 0 0]; %init conditions
opts = odeset('RelTol',1e-5,'AbsTol',1e-5);

%note
%x(1) = i_q
%x(2) = i_d
%x(3) = i_0
%x(4) = w_r
%x(5) = theta_r
%x(6) = integrated control input--closed loop control only
%x(7) = integrated error---closed loop control only

%simulate
[t,x] = ode45(@(t,x) simYASA(t,x,sysParam),tspan,x0,opts);

%Balanced Voltage Set (based off of motor dynamic frequency)
%create time vector
t = t'; %make t a row vector so abcs2qdo doesnt error out

%find step input index
ind = find(t(t<sysParam.StepInputTime),1,'last');

%compute theta
theta = x(:,5);

%compute balanced 3 phase voltages for all time
Batt_volt = sysParam.BattV; %Volts
V_peak = Batt_volt*sqrt(2); %assume RMS value is the same as DC batt value
v_as = V_peak*cos(x(:,5));
v_bs = V_peak*cos(x(:,5)-2/3*pi);
v_cs = V_peak*cos(x(:,5)+2/3*pi);

%create step input in voltage
v_as(1:ind) = 0;
v_bs(1:ind) = 0;
v_cs(1:ind) = 0;

%transform to rotor ref frame that is ELECTRICAL angular velocity of
%rotor
[v_q,v_d,v_0] = abcs2qd0(theta,v_as',v_bs',v_cs');

%place in struct()
V_phase.q = v_q;
V_phase.d = v_d;
V_phase.zero = v_0;

%formula for calculating electrical angular vel of brushless dc machine at ss (from Ch8 of Krause
%EMD--mechanical rotor rotation is 1/(Pole_pairs/2) less
w_r_SS = sysParam.Pole_pairs/2*v_q(end)/sysParam.lambda_m

%% Plot OL Results

%plot 3phase voltages
figure
plot(t,v_as)
hold on
plot(t,v_bs)
plot(t,v_cs)
title('3-phase stator voltages')
xlabel('time-sec')
ylabel('Volts')
legend('v_{as}','v_{bs}','v_{cs}')

%plot i_as, i_bs, i_cs
%transform to stator ref frame
[i_as, i_bs, i_cs] = qd02abcs(x(:,5),x(:,1)',x(:,2)',x(:,3)');

figure
plot(t,i_as)
hold on
plot(t,i_bs)
plot(t,i_cs)
title('3-phase stator currents')
xlabel('time-sec')
ylabel('Current-A')
legend('i_{as}','i_{bs}','i_{cs}')

%plot 3phase rotor ref frame voltages
figure
plot(t,v_q)
hold on
plot(t,v_d)
plot(t,v_0)
title('3-phase qd0 voltages')
xlabel('time-sec')
ylabel('Volts')
legend('v_{q}','v_{d}','v_{0}')

%plot 3phase currents in rotor ref frame
figure
plot(t,x(:,1))
hold on
plot(t,x(:,2))
plot(t,x(:,3))
title('3-phase qd0 currents')
xlabel('time-sec')
ylabel('Current-A')
legend('i_{q}','i_{d}','i_{0}')

%plot rotor angular velocity and torqe and torque v wr
figure
subplot(3,1,1)
plot(t,x(:,4))%where x(4) is the mechanical rotor angular vel
ylabel('Vel-rad/s')
title('Free Acceleration Torque and Angular Velocity')
subplot(3,1,2)
plot(t,3/2*sysParam.Pole_pairs/2*sysParam.lambda_m*x(:,1)) %torque equation
ylabel('Torque-Nm')
xlabel('time-s')

%create SS torque v wr plot
LoadTorques = linspace(40,400,20)';
for i = 1:length(LoadTorques)
    sysParam.T_L = LoadTorques(i);
    %run for shorter time period
    tspan = [0 2];
    x0 = [0 0 0 0 0];
    opts = odeset('RelTol',1e-10,'AbsTol',1e-10);
    [t,x] = ode45(@(t,x) simYASA(t,x,sysParam),tspan,x0,opts);
    w_r_SS(i) = x(end,4);
    T_SS(i) = 3/2*sysParam.Pole_pairs/2*sysParam.lambda_m*x(end,1);
end

subplot(3,1,3)
plot(w_r_SS,T_SS)
ylabel('Torque-Nm')
xlabel('Rotor Vel-rad/s')
title('SS Torque vs. w_r')

%% Closed Loop System Speed Control
clear
% clc
% close all

%PID feedback gains to try [P I D]
Gain = [50 4 10;5e2 4e1 8e1;5e3 4e2 8e2; 5e5 4e4 5];

%references to track [amplitude offset frequency] %as in r(t) = amplitude*sin(freq*t)+offset
speed = [50 100 4;125 150 4; 100 250 1.5]; 

for k = 1:size(speed,1)
    
    figure
    for j = 1:length(Gain)

        %system parameters
        sysParam.Lls = 1e-3; %henries
        sysParam.Lms = 10e-3; %henries
        sysParam.lambda_m = .8; %V*s/rad
        sysParam.R = 1; %ohm
        sysParam.m = 5; %kg
        sysParam.r_out = .294; %m
        sysParam.r_in = .04; %m
        sysParam.J = 1/2*sysParam.m*(sysParam.r_out^2-sysParam.r_in^2); %kg*m^2
        sysParam.B_m = 0.0;
        sysParam.Pole_pairs = 4;
        sysParam.T_L = 0; %Nm
        sysParam.freq = 300; %rad/s
        sysParam.BattV = 340; %Volts
        sysParam.StepInputTime = .1;%sec

        %reference to track
        ref_fxn = @(t) speed(k,1)*sin(speed(k,3)*t)+speed(k,2); %these are called MATLAB anonymous functions
        ref_fxn_deriv = @(t) speed(k,1)*speed(k,3)*cos(speed(k,3)*t);
        ref = {ref_fxn,ref_fxn_deriv}; %can place functions into a cell array to pass to simulation

        %current feedback gains
        K = Gain(j,:); %best K is [5e6 4e5 5]--takes a while to run high gain values

        %simulate axial flux motor
        tspan = [0 8];
        x0 = [0 0 0 0 0 0 0]; %note x(6) and x(7) appear here
        opts = odeset('RelTol',1e-8,'AbsTol',1e-8);
        [t,x] = ode45(@(t,x) simYASA_spdCtrl(t,x,sysParam,K,ref),tspan,x0,opts);

        %mssg
        fprintf('Feedback Gains = %d & %d & %d simulated.\n',Gain(j,1),Gain(j,2),Gain(j,3))

        %plot results
        hold on
        plot(t,x(:,4))
        name{j} = ['PID Fdbk Gains = ' num2str(Gain(j,1)) ' & ' num2str(Gain(j,2)) ' & ' num2str(Gain(j,3))]; 

    end
    
    %mssg
    fprintf('Speed = %d with amplitude = %d and frequency = %d simulated.\n',speed(k,2),speed(k,1),speed(k,3))
    
    %create ref data to plot--easier to create here than to pass from
    %simulation
    refData = zeros(length(t),1);
    %apply step input
    ind = find(t<sysParam.StepInputTime,1,'last');
    refData(1:ind) = 0;
    %create sinusoid
    refData(ind+1:end) = ref_fxn(t(ind+1:end));
    
    %continue plotting
    plot(t,refData)
    name{j+1} = ['Reference'];
    xlabel('time-sec')
    ylabel('Rotor Vel-rad/s')
    legend(name,'location','southeast')
    title(['Max commanded speed = ' num2str(max(ref_fxn(t))) ' @ freq = ' num2str(speed(k,3))])
    
    %determine control input in volts
    controlInput(1:length(t),k) = gradient(x(:,6))./gradient(t); %numerical derivative. only way to get data from sim is to integrate it, retrieve as x(:,6) and differentiate
    controlInput(controlInput>sysParam.BattV) = sysParam.BattV; %numerical derivative results in false control input values above saturation value. Remove these. Note some project plots are created by increasing saturation to sysParam.BattV*sqrt(2)
    name2{k} = ['Speed = ' num2str(max(ref_fxn(t))) ' @ freq = ' num2str(speed(k,3))];
end

%% Plot CL Results

%plot control input
figure
hold on
plot(t,controlInput(1:length(t),:))
xlabel('time-sec')
ylabel('Control Input, V_q-Volts')
legend(name2,'location','southeast')
title(['PID Feedback Gains = ' num2str(Gain(j,1)) ' & ' num2str(Gain(j,2)) ' & ' num2str(Gain(j,3))])

%plot v_as, v_bs, v_cs for last simulation
[v_as, v_bs, v_cs] = qd02abcs(x(:,5),controlInput(1:length(x(:,5))),zeros(1,length(x(:,5))),zeros(1,length(x(:,5))));
figure
subplot(3,1,1)
plot(t,v_as)
ylabel('v_{as}')
title(['Stator voltages for speed = ' num2str(max(ref_fxn(t))) ' @ freq = ' num2str(speed(k,3))])
subplot(3,1,2)
plot(t,v_bs)
ylabel('v_{bs}')
subplot(3,1,3)
plot(t,v_cs)
ylabel('v_{cs}')
xlabel('time-sec')

%plot power for last simulation
%transform to stator ref frame
[i_as, i_bs, i_cs] = qd02abcs(x(:,5),x(1:length(x(:,5)),1)',x(1:length(x(:,5)),2)',x(1:length(x(:,5)),3)');

%calculate instantaneous power
P_as = v_as.*i_as;
P_bs = v_bs.*i_bs;
P_cs = v_cs.*i_cs;

figure
plot(t,P_as)
hold on
plot(t,P_bs)
plot(t,P_cs)
xlabel('time-sec')
ylabel('Power-Watts')
title(['Instantaneous power for speed = ' num2str(max(ref_fxn(t))) ' @ freq = ' num2str(speed(k,3))])
legend('as phase','bs phase','cs phase')

%plot total power
P_total = P_as+P_bs+P_cs;

figure
plot(t,P_total/1e3)
xlabel('time-sec')
ylabel('Power-kW')
title(['Instantaneous total power for speed = ' num2str(max(ref_fxn(t))) ' @ freq = ' num2str(speed(k,3))])

%% Functions
%
% <include>simYASA.m</include>
%
% <include>abcs2qd0.m</include>
%
% <include>qd02abcs.m</include>
%
% <include>simYASA_spdCtrl.m</include>
