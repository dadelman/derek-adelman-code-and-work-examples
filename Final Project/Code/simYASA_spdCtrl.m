function [dxdt] = simYASA_spdCtrl(t,x,sysParam,K,ref)

% x(1) = i_q
% x(2) = i_d
% x(3) = i_0
% x(4) = w_r
% x(5) = theta_r
% x(6) = integrated control input
% x(7) = integrated error

%system parameters
Lls = sysParam.Lls; %henries
Lms = sysParam.Lms; %henries
lambda_m = sysParam.lambda_m; %V*s/rad
R = sysParam.R; %ohm
m = sysParam.m; %kg
r_out = sysParam.r_out; %m
r_in = sysParam.r_in; %m
J = sysParam.J; %kg*m^2
B_m = sysParam.B_m;
Pole_pairs = sysParam.Pole_pairs;
T_L = sysParam.T_L; %Nm

%compute theta in reference frame of the ROTOR
theta = x(5);

%compute balanced 3 phase voltages for all time
Batt_volt = sysParam.BattV; %Volts
V_peak = Batt_volt*sqrt(2);
v_as = V_peak*cos(x(5));
v_bs = V_peak*cos(x(5)-2/3*pi);
v_cs = V_peak*cos(x(5)+2/3*pi);

%transform to rotor ref frame that has ELECTRICAL angular velocity of
%rotor
[v_q,v_d,v_0] = abcs2qd0(theta,v_as,v_bs,v_cs);
V_phase.q = v_q;
V_phase.d = v_d;
V_phase.zero = v_0;

%torque eqn1
T_e = 3/2*Pole_pairs/2*lambda_m*x(1);

%perform step change
if(t<sysParam.StepInputTime)
    
    V_phase.q=0;
    V_phase.d=0;
    V_phase.zero=0;
    T_L = 0;
    
    %linear control law
    v  = 0;
else
    
    %linear control law
    %error = x(4)-r(t)
    %error_dot = dxdt(4)-r_dot(t)
    %integratedError = integral(x(4)-ref{1}(t)) = x(7)
    %v = -proportionalGain*(error)-derivativeGain*(error_dot)-integralGain*integratedError
    v = -K(1)*(x(4)-ref{1}(t))-K(2)*((T_e - B_m*2/Pole_pairs*x(4) - T_L)*(J*2/Pole_pairs)^-1-ref{2}(t))-K(3)*(x(7));
end

%nonlinear control law--see derivation
alpha = 3/2*(sysParam.Pole_pairs/2)^2*sysParam.lambda_m/sysParam.J;
beta = T_L/(J*2/sysParam.Pole_pairs);
alpha_hat = (-alpha*(-sysParam.R/(Lls+3/2*Lms)*x(1)-x(3)*x(2)-x(3)*sysParam.lambda_m/(Lls+3/2*Lms))+B_m/J*(alpha*x(1)-B_m/J*x(3)-beta))/(alpha/(Lls+3/2*Lms));
beta_hat = 1/(alpha/(Lls+3/2*Lms));
u = alpha_hat + beta_hat*v;

%control law saturation
if(abs(u)>sysParam.BattV)
    %fprintf('Warning: Control voltage saturated.\n')
    u = sign(u)*sysParam.BattV;
end

%state equations
dxdt(1) = (-R*x(1) - x(4)*(Lls+3/2*Lms)*x(2) - x(4)*lambda_m + u)*(Lls+3/2*Lms)^-1;
dxdt(2) = (-R*x(2) + x(4)*(Lls+3/2*Lms)*x(1) + V_phase.d)*(Lls+3/2*Lms)^-1;
dxdt(3) = (-R*x(3) + V_phase.zero)*(Lls)^-1;
dxdt(4) = (T_e - B_m*2/Pole_pairs*x(4) - T_L)*(J*2/Pole_pairs)^-1;
dxdt(5) = x(4); %integrate omega to get total radians electrically turned by rotor
dxdt(6) = u; %integrate u to retrieve control input outside of simYASA_spdCtrl
dxdt(7) = x(4)-ref{1}(t); %integrate the error for PID control

dxdt = dxdt';

end

