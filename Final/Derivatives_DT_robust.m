function dxdt = Derivatives_DT_robust(X,u,params,PolyModel_params)

% loop counter from constraints_robust.m
global i

%extract states
x1_states = X(:,1);
x2_states = X(:,2);

%derivatives (computes for all times for ith system)
dxdt(:,1) = x2_states;
dxdt(:,2) = PolyModel_params(1,i)*(1-x1_states.^2).*x2_states-x1_states+u;
end