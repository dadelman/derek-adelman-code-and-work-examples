function dxdt = Derivatives_DT_trapz(X_k,u_k,params,z,i)
%NOT USED IN FINAL.m
global ind
% compute control using spline interpolation
u = z(params.u);

%derivatives
dxdt = params.A{i}*X_k.'+params.B{i}*u_k.';

end