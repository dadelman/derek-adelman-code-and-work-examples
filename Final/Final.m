%% Derek Adelman SYSE 580 Final
%%
% Hi Derek,
%  
% P1 (+5)
%  
% P2b (-1): Slight error in the linearized A matrix.
%  
% P2c (-1):  The domain of validity of this control is limited to |x1| <
% pi/2. Therefore, we cannot make the equilibrium point globally
% asymptotically stable.
%  
% P3b (-1): Some minor issues with your presented steps but the general
% forms and conclusions for both differential equations are consistent with
% the correct solution. The auxiliary differential equation should be this:
% -dot{G} = A^T G - P B R^{-1} B^T G + P d
%  
% P4b: Great work! I would encourage you to try a larger gain value of, say
% Kp =100. You will get much better tracking results, and it becomes more
% obvious when the control fails to keep the system near the nominal
% optimal control solution. Obviously, a higher Kp gain might cause
% problems with noisy x2 (but it was assumed perfect in the problem), and
% there might be high control signals (but they are bounded with saturation
% constraints).
%  
% P4d (+10): Same recommendation as P3b above. Your robust solution with Kp
% = 100 produces excellent tracking of x2 for all the state values.
%  
% Great work throughout the semester!
%  
% Best,
%  
% Dr. Herber
clear 
clc 
format compact
close all
%% Problem 2
%% a-See Handwork

syms x1 x2 a b u c theta lambda

%companion form
x = [x1;x2];
x_dot = [x2;a*sin(x1)-b*u*cos(x1)-c*x2];



%% b-See Handwork

%find equilibrium u point for [theta 0]
u_eq = solve(subs(x_dot,[sym('x1') sym('x2')],[theta 0])==0,u)

%jacobian linearization
A = subs(jacobian(x_dot,x),[sym('x1') sym('x2') sym('u')],[theta 0 u_eq])
B = subs(jacobian(x_dot,u),[sym('x1') sym('x2') sym('u')],[theta 0 u_eq])

%% c-See Handwork

syms k1 k2 lambda

%simulate jacobian linearized system
a = 1;
b = 2;
c = 3;
th = pi/4;
u_eq_num = double(subs(u_eq,[sym('a') sym('b') sym('c') sym('theta')],[a b c th]))
A_jac = double(subs(A,[sym('a') sym('b') sym('c') sym('theta') sym('u_eq')],[a b c th u_eq_num]))
B_jac = double(subs(B,[sym('a') sym('b') sym('c') sym('theta') sym('u_eq')],[a b c th u_eq_num]))

%check stability
eig(A_jac) %linearized system is not stable about x = [theta 0] for all theta

%create phase plane
PhasePlane(@(t,x) A_jac*[x(1);x(2)],pi,pi,[1 0;pi/4 0;pi/2 0])
title('Jacobian linearized system')

%stabilize system
K_jac = place(A_jac,B_jac,[-5+5i -5-5i]);

%stabilized phase plane
PhasePlane(@(t,x) (A_jac-B_jac*K_jac)*[x(1);x(2)],pi,pi,[1 0;pi/4 0;pi/2 0])
title('Fdbk Stabilized Jac Lin system')
%simulate feedback linearized system
K = [k1;k2];  

%symbolic A matrix (from handwork)
A = [0 1;-k1 -k2];

%eigenvalues
EigVals = solve(det(A-lambda*(eye(size(A))))==0,lambda)

%choose k from conditions derived in handwork
K = [20 20];

%new A matrix
A = [0 1;-K(1) -K(2)];

%check eigvals
eig(A)

%input for matrix form
B = [0;K(1)];

%parameters
r = 0; %theta to control to
a = 1;
b = 2;
c = 3;

%sim params
tspan = [0 10];
x0 = [pi/4;0;pi/4;0];
opts = odeset('reltol',1e-10,'abstol',1e-10);

%sim
[t,x] = ode45(@(t,x) Prob2c(t,x,a,b,c,K,A,B,r),tspan,x0,opts);

%plot results
figure;hold on
subplot(2,1,1)
plot(t,x(:,1:2))
ylabel('states')
title('Fdbk linearization via Equations')
subplot(2,1,2)
plot(t,x(:,3:4))
xlabel('time, s')
ylabel('states')
legend('x_1','x_2')
title('Fdbk linearization  via State Space')



%% d-See Handwork

%% Problem 4
%% a

%parameters
params.x0 = [1;0];
params.tf = 15; %time increased to better show system stabilizing behavior
params.mu = 1;

%Dirct transcription parameters
NumTimePoints = 200;%number of tme points
params.numTime = NumTimePoints; %this number needs to be the same as part 4d
params.t = linspace(0,params.tf,params.numTime).';
params.h = diff(params.t); %this is a fixed step
params.numControls = 1; %number of controls
params.numStates = 2; %number of states

%discritize variable indices for control variable AND STATES
params.x1 = [1:params.numTime].';
params.x2 = [params.x1(end)+1:2*params.numTime].';
params.u = [params.x2(end)+1:3*params.numTime].';

%initial guess for control input, u (multiplication for the case in which
%there are multiple control values) AND % initial guess for all optimization variables (all zeros)
z0 = ones(params.numTime*(params.numControls+params.numStates),1)*1;

% fmincon options
options = optimoptions(@fmincon,'display','final','MaxFunEvals',10e4,...
    'UseParallel',true);

%optimize the control
tic
z = fmincon(@(z) objectiveFxn_DT(z,params),z0,[],[],[],[],[],[],@(z) constraints(z,params),options);
toc

%extract control and state values from z
results.u = z(params.u);
results.x1 = z(params.x1);
results.x2 = z(params.x2);

%simulate optimal soln on denser time grid
tspan = [0 params.tf];
opts = odeset('reltol',1e-10,'abstol',1e-10);
[t,x] = ode45(@(t,x) Derivatives(t,x,results.u,params),tspan,params.x0,opts);

%plot results
figure
subplot(2,1,1);hold on
plot(t,x)
plot(params.t,z(params.x1),'o')
plot(params.t,z(params.x2),'x')
title('Optimal Control')
ylabel('states')
legend('Simulated x_1','simulated x_2','optimized x_1','optimized x_2','location','southwest')
subplot(2,1,2);hold on
plot(params.t,z(params.u),'o')
plot(t,interp1(params.t,results.u,t,'spline'));
ylabel('control')
legend('optimized control','interp control')
xlabel('time, s')

%note that regulation performance increases as the number of optimized
%points increases
%% b

%nominal setup
Kp = 100;

%sim params
tspan = [0 params.tf];
x0 = [params.x0;0];
opts = odeset('reltol',1e-6,'abstol',1e-6); %ode23 and higher tolerance used for stability of derivative terms used later on

%sim nominal system with opt control and Kp
[t_nominal,x_nominal] = ode23(@(t,x) Prob4b(t,x,results.u,results.x2,params,Kp),tspan,x0,opts);

%plot nominal system behavior
figure
subplot(2,1,1);hold on
plot(t_nominal,x_nominal(:,1:2))
ylabel('states')
legend('x_1','x_2')
title(['Nominal with P-control Kp = ' num2str(Kp)])
subplot(2,1,2);hold on
plot(t_nominal,gradient(x_nominal(:,3))./gradient(t_nominal))
ylabel('control')
xlabel('time, s')

%variation setup
mu = [0.6 0.8 1.0 1.2 1.4 1.6 1.8];
x1_0 = [0.9 1.0 1.1];

%6 plots in total produced, two for Kp state and control, 2 for opt only
%state and control, and 2 for objective fxn val for Kp and opt only
%Kp controller state figure
fig1 = figure('WindowState','maximized');
%Kp controller control figure
fig2 = figure('WindowState','maximized');
%nominal opt controller state figure
fig3 = figure('WindowState','maximized');
%nominal opt controller control figure
fig4 = figure('WindowState','maximized');
count = 1 ;

for i = 1:length(mu)
    for j = 1:length(x1_0)

    %set current mu
    params.mu = mu(i);
    
    %set current IC
    x0 = [x1_0(j);0;0]; %third state is to extract control u
    
    %sim params
    tspan = [0 params.tf];
    opts = odeset('reltol',1e-6,'abstol',1e-6);
    
    %simulate kp controller and opt only controller (Prob4b is Kp and
    %Prob4b_u is opt only)
    [t,x] = ode23(@(t,x) Prob4b(t,x,results.u,results.x2,params,Kp),tspan,x0,opts);
    [t_nomU,x_nomU] = ode45(@(t,x) Prob4b_u(t,x,results.u,results.x2,params,Kp),tspan,x0,opts);

    %plot kp states
    figure(fig1)
    subplot(2,1,1);hold on
    plot(t,x(:,1))
    subplot(2,1,2);hold on
    plot(t,x(:,2))
    
    %legend labels
    name_fig1{count} = ['mu = ' num2str(mu(i)) ' x_10 = ' num2str(x1_0(j))];
        
    %plot kp control
    figure(fig2);hold on
    u = gradient(x(:,3))./gradient(t);
    plot(t,u)
    
    %objective fxn value array for Kp-type control
    ObjFxnVal_kp(i,j) = trapz(t,x(:,1).^2+x(:,2).^2+u.^2);
    
    %plot Unom states
    figure(fig3)
    subplot(2,1,1);hold on
    plot(t_nomU,x_nomU(:,1))
    subplot(2,1,2);hold on
    plot(t_nomU,x_nomU(:,2))
    
    %plot Unom control
    figure(fig4);hold on
    u = gradient(x_nomU(:,3))./gradient(t_nomU);
    plot(t_nomU,u)
    
    %obj fxn val array for opt only
    ObjFxnVal_nom(i,j) = trapz(t_nomU,x_nomU(:,1).^2+x_nomU(:,2).^2+u.^2);
    
    %compare obj fxn values
    tol = .01;
    if(ObjFxnVal_nom(i,j)-ObjFxnVal_kp(i,j)>tol)
        ObjFxnVal_compare_kp2nom{i,j} = 'Better';
    elseif(abs(ObjFxnVal_nom(i,j)-ObjFxnVal_kp(i,j))<tol)
        ObjFxnVal_compare_kp2nom{i,j} = 'Same';
    else
        ObjFxnVal_compare_kp2nom{i,j} = 'Worse';
    end
    
    %for legend labels
    count = count+1;
    
    end
end

%append fig 1
figure(fig1)
subplot(2,1,1)
title(['State Plot Kp = ' num2str(Kp)])
ylabel('x_1')
legend(name_fig1,'location','southoutside','NumColumns',7)
subplot(2,1,2)
ylabel('x_2')
xlabel('time, s')

%append fig 2
figure(fig2)
xlabel('time, s')
ylabel('control')
title(['Control Plot Kp = ' num2str(Kp)])
legend(name_fig1,'location','southoutside','NumColumns',7)

%append fig 3
figure(fig3)
subplot(2,1,1)
title('State Plot Nominal')
ylabel('x_1')
legend(name_fig1,'location','southoutside','NumColumns',7)
subplot(2,1,2)
ylabel('x_2')
xlabel('time, s')

%append fig 4
figure(fig4)
xlabel('time, s')
ylabel('control')
title('Control Plot Nominal')
legend(name_fig1,'location','southoutside','NumColumns',7)

%create surfaces for objective function value
%kp controller
[X,Y] = meshgrid(mu,x1_0);
Z = ObjFxnVal_kp.';
figure
surf(X,Y,Z)
title('Objective Fxn Val for Kp-type')
xlabel('mu')
ylabel('x1_0')
zlabel('Obj Fxn Val')

%nominal controller
[X,Y] = meshgrid(mu,x1_0);
Z = ObjFxnVal_nom.';
figure
surf(X,Y,Z)
title('Objective Fxn Val for Nominal')
xlabel('mu')
ylabel('x1_0')
zlabel('Obj Fxn Val')

ObjFxnVal_compare_kp2nom

%% c
%% Comment
% The P-type controller encorportates feedback into the control law, so
% when the actual states deviate from the open-loop optimal states, the
% P-type controller adjusts the control until it returns to optimal. Since
% the open loop control has no feedback, when the state trajectory deviates
% from optimum due to uncertainties it has no way to correct and the states
% soon return to unforced behavior as the optimal control goes to zero.
%% Comment
% For uncertainties near the nominal plant the P-type controller will
% produce reasonable results as it will drive the states towards their
% optimal trajectories. For uncertainties far from nominal (or more
% accurately, uncertainties that result in more overshoot of the states)
% the saturated control limits the ability of the controller to return
% deviated states to their optimal trajectories. These trajectories are no
% longer "reasonable" because they do not track near the optimal state
% trajectories and by the time the P part of the controller is no longer
% saturation-limited, the optimal states are already at zero, turning
% performance into nothing more than a purely P controller. Regardless, it
% is an improvement over open-loop optimal.
%% Comment
% The controller will produce suboptimal results (not truly optimal for any
% points other than nominal). By suboptimal it is meant that true
% optimality is only accomplished by performing an optimization for each
% set of non-nominal plant parameters. This is because the optimal control
% for the nominal plant is not the same as the optimal control for the
% deviated plants, so as the P-type controller pushes the control to the
% optimal control, it is pushing it towards what is optimal for a certain
% plant and not the deviated one. However, there are sections where the
% optimal control for the nominal plant is "close" to what the true optimal
% control for the deviated plant would be. In this sense, the control
% is suboptimal--applying an optimal control of one plant to similar plants
% to obtain results that are "close" to optimal.
%% Comment
% The problem can be recast as a robust optimal control problem by
% optimizing instead for the worst performing plant in the uncertainty set
% instead of the nominal plant. This would then guarantee a worst case
% performance for the optimal control, see part d. The main difference is
% that several optimal control problems would be solved and the objective
% function would be modifed so as to choose the maximum objective function
% of the optimized plant set. From a stochastic perspective, the objective
% function would remained unchanged, however the constraints would be
% adjusted to include noise in the system. Adding process noise to the
% state equations and initial conditions, turns the problem into an LQG
% one.

%% d
% Two performance improvements are suggested, one involving adding I,D
% terms in the controller (all for state 2), the other performing robust
% optimal control to determine the optimal control of the most deviated
% plant and using that with the PID controller instead of the nominal
% optimal control law. Both will be implemented.

%% PID-type controller
% Add PID
% close all

%nominal setup
%PID terms
Kp = 100;
Ki = 10;
Kd = .25;

%reset mu
params.mu = 1;

%var to implement in derivative term
global error_prev
error_prev = 0;

%var to implement in derivative term
global t_prev
t_prev = 0;

%sim params
tspan = [0.0001 params.tf];
x0 = [params.x0;0;0]; %third state is for control, fourth is integrator
opts = odeset('reltol',1e-6,'abstol',1e-6);

%sim params
[t,x] = ode23(@(t,x) Prob4d(t,x,results.u,results.x1,results.x2,params,Kp,Ki,Kd),tspan,x0,opts);

%plot results of nominal system behavior with PID and nominal opt control
figure
subplot(2,1,1);hold on
plot(t,x(:,1:2))
ylabel('states')
legend('x_1','x_2')
title(['Nominal with PID-control Kp = ' num2str(Kp) ' Ki = ' num2str(Ki) ' Kd = ' num2str(Kd)])
subplot(2,1,2);hold on
plot(t,gradient(x(:,3))./gradient(t))
ylabel('control')
xlabel('time, s')

%variation setup
mu = [0.6 0.8 1.0 1.2 1.4 1.6 1.8];
x1_0 = [0.9 1.0 1.1];

%same plot setup as for part b, except nominal optimal control only plots
%are not plotted
%Kp controller state figure
fig5 = figure('WindowState','maximized');
%Kp controller control figure
fig6 = figure('WindowState','maximized');

count = 1 ;

for i = 1:length(mu)
    for j = 1:length(x1_0)
        
    %reset derivative vars for each sim loop
    error_prev = 0;
    t_prev = 0;
    
    %set current me
    params.mu = mu(i);
    
    %set current IC
    x0 = [x1_0(j);0;0;0]; %third state is for control, fourth is integrator
    
    %sim params
    tspan = [0.001 params.tf];
    opts = odeset('reltol',1e-6,'abstol',1e-6);
    
    %sim
    [t,x] = ode23(@(t,x) Prob4d(t,x,results.u,results.x1,results.x2,params,Kp,Ki,Kd),tspan,x0,opts);

    %plot kpi states
    figure(fig5)
    subplot(2,1,1);hold on
    plot(t,x(:,1))
    subplot(2,1,2);hold on
    plot(t,x(:,2))
    
    %legend labels
    name_fig2{count} = ['mu = ' num2str(mu(i)) ' x_10 = ' num2str(x1_0(j))];
        
    %plot kpi control
    figure(fig6);hold on
    u = gradient(x(:,3))./gradient(t);
    plot(t,u)
    
    ObjFxnVal_kpi(i,j) = trapz(t,x(:,1).^2+x(:,2).^2+u.^2);
    
    %compare obj fxn values
    tol = .01;
    if(ObjFxnVal_kp(i,j)-ObjFxnVal_kpi(i,j)>tol)
        ObjFxnVal_compare{i,j} = 'Better';
    elseif(abs(ObjFxnVal_kp(i,j)-ObjFxnVal_kpi(i,j))<tol)
        ObjFxnVal_compare{i,j} = 'Same';
    else
        ObjFxnVal_compare{i,j} = 'Worse';
    end
    
    %for legend labels
    count = count+1;
    
    end
end

%append fig 5
figure(fig5)
subplot(2,1,1)
title(['State Plot Kp = ' num2str(Kp) ' Ki = ' num2str(Ki) ' Kd = ' num2str(Kd)])
ylabel('x_1')
legend(name_fig2,'location','southoutside','NumColumns',7)
subplot(2,1,2)
ylabel('x_2')
xlabel('time, s')

%append fig 6
figure(fig6)
xlabel('time, s')
ylabel('control')
title(['Control Plot Kp = ' num2str(Kp) ' Ki = ' num2str(Ki) ' Kd = ' num2str(Kd)])
legend(name_fig2,'location','southoutside','NumColumns',7)

%create surfaces for objective function value
%kpi controller
[X,Y] = meshgrid(mu,x1_0);
Z = ObjFxnVal_kpi.';
figure
surf(X,Y,Z)
title('Objective Fxn Val for Kp Ki Kd')
xlabel('mu')
ylabel('x1_0')
zlabel('Obj Fxn Val')

ObjFxnVal_compare
%% Comment
% It is seen that adding ID terms increases performance for some deviated
% plants but not others. This is seen in the ObjFxnVal_compare matrix
% where 9 of the plants have performance improvement. The other plants had
% roughly the same performance. Note that too far in either direction with
% Ki or Kd can lead to overall worse performance than with just Kp. The
% implementation of the D term is poor, and as such its value is limited to
% ~1 before the simulation becomes unstable and takes a long time/delivers
% poor results

%% Robust optimal control
% Develop robust optimal control from the polytopic approach

%clear some params...helps with debugging
clear params
clear LagrangeTerm_robust
clear objectiveFxn_DT_robust
clear ind indMin
% close all

%variation setup
mu = [0.6 0.8 1.0 1.2 1.4 1.6 1.8];
x1_0 = [0.9 1.0 1.1];

%it was determined thru trial and error that the max plant deviation
%combination was an upper bound on the state envelope and optimizing only
%around the max deviation gave the best improvement versus optimizing
%around all the extreme plants

%deviations--simply add the min terms to optimize around all polytopic
%models
v1 = [max(mu)];% min(mu)];
v2 = [max(x1_0)];% min(x1_0)];
PolyModel_params = combvec(v1,v2);

%parameters
params.x0 = [1;0];
params.tf = 15;
params.mu = 1;

%Dirct transcription parameters
%number of tme points
params.numTime = NumTimePoints;
params.t = linspace(0,params.tf,params.numTime).';
params.h = diff(params.t); %this is a fixed step
params.numControls = 1; %number of controls
params.numStates = 2; %number of states

%for when running optimization around all polytopic models
MxFxnEvals = 35e4;

%vars for extraction from objective function
global ind
global indMin
global LagrangeTerm_robust

%discritize variable indices for control variable AND STATES
for i = 1:size(PolyModel_params,2)
    if(i==1)
        params.x1{i} = [1:params.numTime].';
%         params.x1{i}
        params.x2{i} = [params.x1{i}(end)+1:2*params.numTime].';
%         params.x2{i}
    else
        params.x1{i} = [params.x2{i-1}(end)+1:params.x2{i-1}(end)+params.numTime].';
%         params.x1{i}
        params.x2{i} = [params.x1{i}(end)+1:params.x1{i}(end)+params.numTime].';
%         params.x2{i}
    end
end
params.u = [params.x2{end}(end)+1:params.x2{end}(end-1)+1+params.numTime].';

%perform optimization
%initial guess
z0 = ones(params.u(end),1)*1e-3;

% fmincon options
options = optimoptions(@fmincon,'display','final','MaxFunEvals',MxFxnEvals,...
    'UseParallel',true);

%optimize the control
tic
z = fmincon(@(z) objectiveFxn_DT_robust(z,params,PolyModel_params),z0,[],[],[],[],[],[],@(z) constraints_robust(z,params,PolyModel_params),options);
toc

%extract control and state values from z
results_robust.u = z(params.u); %there is only one control for all plants
for i = 1:size(PolyModel_params,2)
    results_robust.x{i} = [z(params.x1{i}) z(params.x2{i})];
end

%print obj fxn value
ObjFxnVal = LagrangeTerm_robust;

%find indices of worst and best models (in single plant case these are both
%1)
% W = find(ObjFxnVal==max(ObjFxnVal))
W = ind;
% B = find(ObjFxnVal==min(ObjFxnVal))
B = indMin;

%only used for if there is more than one polytopic model
%simulate worst model-- run this control through all models
% figL = figure;
% for j = 1:size(PolyModel_params,2)
%     
%     tspan = [0 params.tf];
%     opts = odeset('reltol',1e-6,'abstol',1e-6);
%     [time,states] = ode23(@(t,x) Derivatives_robust(t,x,results_robust.u,PolyModel_params,B,params),tspan,params.x0,opts);
% 
%     T{j} = time;
%     States{j} = states;
% 
% 
%     name{j} = ['model ' num2str(j)];
%     figure(figL)
%     subplot(2,1,1); hold on
%     plot(T{j},States{j}(:,1))
%     subplot(2,1,2); hold on
%     plot(T{j},States{j}(:,2))
% 
% end

%nominal sys
tspan = [0 params.tf];
opts = odeset('reltol',1e-6,'abstol',1e-6);
[time_nom,states_nom] = ode23(@(t,x) Derivatives(t,x,results.u,params),tspan,params.x0,opts);

%used if there is more than one polytopic model
% name{end+1} = 'nominal';
% figure(figL); hold on
% subplot(2,1,1); hold on
% plot(time_nom,states_nom(:,1))
% subplot(2,1,2); hold on
% plot(time_nom,states_nom(:,2))
% legend(name)


%simulate optimal soln on denser time grid
%simulate best model
tspan = [0 params.tf];
opts = odeset('reltol',1e-6,'abstol',1e-6);
[t_B,x_B] = ode23(@(t,x) Derivatives_robust(t,x,results_robust.u,PolyModel_params,B,params),tspan,[PolyModel_params(2,B);0],opts);


%worst system
tspan = [0 params.tf];
opts = odeset('reltol',1e-6,'abstol',1e-6);
[t_W,x_W] = ode23(@(t,x) Derivatives_robust(t,x,results_robust.u,PolyModel_params,W,params),tspan,[PolyModel_params(2,W);0],opts);

%plot state results
figure
subplot(2,1,1);hold on
%nominal system
plot(time_nom,states_nom(:,1))
%worst system
plot(params.t,results_robust.x{W}(:,1),'x')
plot(t_W,x_W(:,1))
%best system
plot(params.t,results_robust.x{B}(:,1),'o')
plot(t_B,x_B(:,1))
ylabel('\itOutput, x1')
legend('Nominal System','Worst, Worst Case Opt. Pts','WW Case Interp','Best, Worst Case Opt. Pts','BW Case Interp')
title('Robust Optimal Control')
subplot(2,1,2);hold on
%nominal system
plot(time_nom,states_nom(:,2))
%worst system
plot(params.t,results_robust.x{W}(:,2),'x')
plot(t_W,x_W(:,2))
%best system
plot(params.t,results_robust.x{B}(:,2),'o')
plot(t_B,x_B(:,2))
ylabel('\itOutput, x2')
legend('Nominal System','Worst, Worst Case Opt. Pts','WW Case Interp','Best, Worst Case Opt. Pts','BW Case Interp')

%plot control results
figure
%nominal opt system control
plot(params.t,results.u,'x');hold on
u_interp = interp1(params.t,results.u,t_nominal,'spline');
plot(t_nominal,u_interp);hold on
%worst system optimal control
plot(params.t,results_robust.u,'o')
u_interp = interp1(params.t,results_robust.u,t_W,'spline');
plot(t_W,u_interp)
title('Robust Optimal Control')
xlabel('\ittime, s')
ylabel('\itControl Input')
legend('Nominal Sys Opt pts','Nominal Sys Interp','Worst System Opt pts','Worst Sys Interp')

%% Comment
% It was determined thru trial and error that the max deviation was an
% upper bound on the state envelope and optimizing only around the max
% deviation gave the best improvement for all systems versus optimizing
% around all the extreme deviations.

%% Robust Control with PID-type controller
% Add PID to robust optimal control 
%close all
%nominal setup
%PID terms
Kp = 100;
Ki = 10;
Kd = 0.1;
params.mu = 1;

%derivative vars
error_prev = 0;
t_prev = 0;

%sim params
tspan = [0.0001 params.tf];
x0 = [params.x0;0;0]; %third state is for control, fourth is integrator
opts = odeset('reltol',1e-6,'abstol',1e-6);

%sim nominal system with PID and robust optimal control
[t,x] = ode23(@(t,x) Prob4d_robust(t,x,results_robust.u,results_robust.x{1}(:,1),results_robust.x{1}(:,2),params,Kp,Ki,Kd),tspan,x0,opts);

%plot results
figure
subplot(2,1,1);hold on
plot(t,x(:,1:2))
ylabel('states')
legend('x_1','x_2')
title(['Nominal with Robust/PID-control Kp = ' num2str(Kp) ' Ki = ' num2str(Ki) ' Kd = ' num2str(Kd)])
subplot(2,1,2);hold on
plot(t,gradient(x(:,3))./gradient(t))
ylabel('control')
xlabel('time, s')

%variation setup
mu = [0.6 0.8 1.0 1.2 1.4 1.6 1.8];
x1_0 = [0.9 1.0 1.1];

%same as before
%Kp controller state figure
fig7 = figure('WindowState','maximized');
%Kp controller control figure
fig8 = figure('WindowState','maximized');
%robust opt controller state figure
fig9 = figure('WindowState','maximized');
%robust opt controller control figure
% fig4 = figure;
count = 1 ;

for i = 1:length(mu)
    for j = 1:length(x1_0)
    
    %reset derivative term vars
    error_prev = 0;
    t_prev = 0;
    
    %set current mu and IC
    params.mu = mu(i);
    x0 = [x1_0(j);0;0;0]; %third state is for control, fourth is integrator
    
    %sim params
    tspan = [0.001 params.tf];
    opts = odeset('reltol',1e-6,'abstol',1e-6);
    
    %sim
    [t,x] = ode23(@(t,x) Prob4d_robust(t,x,results_robust.u,results_robust.x{1}(:,1),results_robust.x{1}(:,2),params,Kp,Ki,Kd),tspan,x0,opts);
    [t_robustOnly,x_robustOnly] = ode23(@(t,x) Prob4d_robust(t,x,results_robust.u,results_robust.x{1}(:,1),results_robust.x{1}(:,2),params,0,0,0),tspan,x0,opts);

    %plot kpi states
    figure(fig7)
    subplot(2,1,1);hold on
    plot(t,x(:,1))
    subplot(2,1,2);hold on
    plot(t,x(:,2))
    
    %for legend labels
    name_fig3{count} = ['mu = ' num2str(mu(i)) ' x_10 = ' num2str(x1_0(j))];
        
    %plot kpi control
    figure(fig8);hold on
    u = gradient(x(:,3))./gradient(t);
    plot(t,u)
    
    figure(fig9)
    subplot(2,1,1);hold on
    plot(t_robustOnly,x_robustOnly(:,1))
    subplot(2,1,2);hold on
    plot(t_robustOnly,x_robustOnly(:,2))
    
    ObjFxnVal_kpi_robust(i,j) = trapz(t,x(:,1).^2+x(:,2).^2+u.^2);
    
    %compare obj fxn values
    %compare kpid robust to just kp
    tol = .01;
    if(ObjFxnVal_kp(i,j)-ObjFxnVal_kpi_robust(i,j)>tol)
        ObjFxnVal_compare_robust{i,j} = 'Better';
    elseif(abs(ObjFxnVal_kp(i,j)-ObjFxnVal_kpi_robust(i,j))<tol)
        ObjFxnVal_compare_robust{i,j} = 'Same';
    else
        ObjFxnVal_compare_robust{i,j} = 'Worse';
    end
    
    %compare kpid to kpid robust
    if(ObjFxnVal_kpi(i,j)-ObjFxnVal_kpi_robust(i,j)>tol)
        ObjFxnVal_compare_robust2_opt{i,j} = 'Better';
    elseif(abs(ObjFxnVal_kpi(i,j)-ObjFxnVal_kpi_robust(i,j))<tol)
        ObjFxnVal_compare_robust2_opt{i,j} = 'Same';
    else
        ObjFxnVal_compare_robust2_opt{i,j} = 'Worse';
    end
    
    %for legend labels
    count = count+1;
    
    end
end

%append fig 7
figure(fig7)
subplot(2,1,1)
title(['Robust State Plot Kp = ' num2str(Kp) ' Ki = ' num2str(Ki) ' Kd = ' num2str(Kd)])
ylabel('x_1')
legend(name_fig3,'location','southoutside','NumColumns',7)
subplot(2,1,2)
ylabel('x_2')
xlabel('time, s')

%append fig 8
figure(fig8)
xlabel('time, s')
ylabel('control')
title(['Robust Control Plot Kp = ' num2str(Kp) ' Ki = ' num2str(Ki) ' Kd = ' num2str(Kd)])
legend(name_fig3,'location','southoutside','NumColumns',7)

%append fig 9
figure(fig9)
subplot(2,1,1)
title(['Robust Only, State Plot Kp = ' num2str(0) ' Ki = ' num2str(0) ' Kd = ' num2str(0)])
ylabel('x_1')
legend(name_fig3,'location','southoutside','NumColumns',7)
subplot(2,1,2)
ylabel('x_2')
xlabel('time, s')

%create surfaces for objective function value
%kpi controller
[X,Y] = meshgrid(mu,x1_0);
Z = ObjFxnVal_kpi_robust.';
figure
surf(X,Y,Z)
title('Objective Fxn Val for Kp Ki Kd Robust')
xlabel('mu')
ylabel('x1_0')
zlabel('Obj Fxn Val')

ObjFxnVal_compare_robust
ObjFxnVal_compare_robust2_opt

%% Comment
% Notice from the plots and comparative matrices that the robust-optimal
% PID control gives the best overall performance for the entire system as
% no states deviate largely from the main state trajectory--trajectory
% variance is reduced across all trajectories (compare robust state plot to
% State plot). The implementation of the derivative term is very poor and
% inefficient, and D cannot be increased much above 1 without simulations
% taking a long time and becoming numerically unstable. Given the plots,
% this is the best control solution of all the previous simulated
% implementations, especially for the most deviated systems. 

%% Functions
%
% <include>Prob2c.m</include>
%
% <include>Prob4b.m</include>
%
% <include>Prob4b_u.m</include>
%
% <include>objectiveFxn_DT.m</include>
%
% <include>constraints.m</include>
%
% <include>Derivatives.m</include>
%
% <include>Derivatives_DT.m</include>
%
% <include>Prob4d.m</include>
%
% <include>Prob4d_robust.m</include>
%
% <include>objectiveFxn_DT_robust.m</include>
%
% <include>constraints_robust.m</include>
%
% <include>Derivatives_robust.m</include>
%
% <include>Derivatives_DT_robust.m</include>
%
% <include>PhasePlane.m</include>


