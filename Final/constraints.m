function [g,h] = constraints(z,params)

% extract controls and states from Z
u = z(params.u);
x1_states = z(params.x1);
x2_states = z(params.x2);

% discretized state matrices %not needed for one state, but would add
% horizontally if there were more
X = [x1_states(:) x2_states(:)];

% state derivatives
derivs = (Derivatives_DT(X,u,params)).';

%DEFECT CONSTRAINTS
zeta = diff(X) - params.h(1)/2*(derivs(:,1:end-1)+derivs(:,2:end)).'; %the integral in zeta is approximated using the trapezoidal rule

% path constraints
g1 = -.3-u;
g2 = u-1;

% combine constraints
g = [g1;g2];
h = [zeta(:);X(1,1)-params.x0(1);X(1,2)-params.x0(2)];

end

