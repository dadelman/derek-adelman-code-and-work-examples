function [g,h] = constraints_robust(z,params,PolyModel_params)

%make i global for easy pass to derivative function
global i
for i = 1:size(PolyModel_params,2)
    % extract controls and states from Z
    u = z(params.u);
    x1_states = z(params.x1{i});
    x2_states = z(params.x2{i});

    % discretized state matrices %not needed for one state, but would add
    % horizontally if there were more
    X = [x1_states x2_states];
    X1(i) = X(1,1);


    % calculate derivatives for defect constraints
    derivs = (Derivatives_DT_robust(X,u,params,PolyModel_params)).';

    %DEFECT CONSTRAINTS
    zeta = diff(X) - params.h(1)/2*(derivs(:,1:end-1)+derivs(:,2:end)).'; %the integral in zeta is approximated using the trapezoidal rule
    zeta1(:,i) = zeta(:);
end
    

% path constraints
g1 = -.3-u;
g2 = u-1;

% combine constraints
g = [g1;g2];
h = [zeta1(:);X1(:)-PolyModel_params(2,:).';X(1,2)-params.x0(2)];

end