function dxdt = Derivatives_DT(X,u,params)

%extract states for all time points
x1_states = X(:,1);
x2_states = X(:,2);

%derivatives for all time points
dxdt(:,1) = x2_states;
dxdt(:,2) = params.mu*(1-x1_states.^2).*x2_states-x1_states+u;
end

