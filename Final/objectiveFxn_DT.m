function J = objectiveFxn_DT(z,params)

%extract indexed control points
controls = z(params.u);
x1_states = z(params.x1);
x2_states = z(params.x2);

%DONT NEED TO RUN DIRECT SIMULATION IN DIRECT TRANSCRIPTION METHOD
% X = runSimulation(controls,params);

%evaluate the objective function
LagrangeTerm = trapz(params.t,x1_states.^2+x2_states.^2+controls.^2); %can also transform to Mayer term. This seems more efficient. Leaving as is to be able to compare to Session 10 exercises.

%mayer term
MayerTerm = 0;

%objective fxn val
J = LagrangeTerm + MayerTerm;

end



