function J = objectiveFxn_DT_robust(z,params,PolyModel_params)

%used global vars for easy extraction
global ind
global indMin
% global LagrangeTerm_robust

%compute lagrange term for all systems using the same control
for i = 1:size(PolyModel_params,2)
    
    %extract indexed control points
    u = z(params.u);
    x1_states = z(params.x1{i});
    x2_states = z(params.x2{i});

    %condense
    X = [x1_states x2_states];

    LagrangeTerm_robust(i) = trapz(params.t,x1_states.^2+x2_states.^2+u.^2); %can also transform to Mayer term. This seems more efficient. Leaving as is to be able to compare to Session 10 exercises.
end

%find the largest and smallest lagrange term
LagrangeTerm_robust;
[LT,ind] = max(LagrangeTerm_robust);
[LTmin,indMin] = min(LagrangeTerm_robust);

MayerTerm = 0;

%obj fxn val
J = LT + MayerTerm;
end