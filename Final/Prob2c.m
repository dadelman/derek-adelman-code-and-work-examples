function [dxdt] = Prob2c(t,x,a,b,c,K,A,B,r)

%change reference to track
if(t>5)
    r=1;
end

%linear control law
v = -K*[x(1)-r;x(2)];

%dynamic inversion control law (see handwork)
u = (-a*sin(x(1))+c*x(2))/(-b*cos(x(1)))+v/(-b*cos(x(1)));

%equations
dxdt(1) = x(2);
dxdt(2) = a*sin(x(1))-b*u*cos(x(1))-c*x(2);

%state space form
dxdt(3:4) = A*x(3:4)+B*r;

dxdt = dxdt.';

end

