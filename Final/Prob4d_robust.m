function dxdt = Prob4d_robust(t,x,discrete_u,discrete_x1,discrete_x2,params,Kp,Ki,Kd)

%derivative terms
global error_prev
global t_prev

%interp x2 for Kp
x2_interp = interp1(params.t,discrete_x2,t,'spline');

%calc derivative of error
deriv_error = (x2_interp-x(2)-error_prev)/(t-t_prev);

% compute control using spline interpolation
u = interp1(params.t,discrete_u,t,'spline') + Kp*(x2_interp-x(2))...
    + Ki*x(4)+Kd*deriv_error;

%control saturation
if(u>=1)
    u = 1;
end
if(u<=-.3)
    u = -.3;
end

%derivatives
dxdt(1) = x(2);
dxdt(2) = params.mu*(1-x(1)^2)*x(2)-x(1)+u;
dxdt(3) = u;
dxdt(4) = x2_interp-x(2);
dxdt = dxdt.';

%increment deriv vars
error_prev = dxdt(4);
t_prev = t-.0001; %needed to avoid division by zero

end