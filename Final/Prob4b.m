function dxdt = Prob4b(t,x,discrete_u,discrete_x2,params,Kp)

% compute control using spline interpolation
u = interp1(params.t,discrete_u,t,'spline') + Kp*(interp1(params.t,discrete_x2,t,'spline')-x(2));
if(u>=1)
    u=1;
end
if(u<=-.3)
    u = -.3;
end
%derivatives
dxdt(1) = x(2);
dxdt(2) = params.mu*(1-x(1)^2)*x(2)-x(1)+u;
dxdt(3) = u;
dxdt = dxdt.';

end