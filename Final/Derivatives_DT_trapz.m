function dxdt = Derivatives_DT_trapz(X,u,params,z)
%NOT USED IN FINAL.m
global mu
% compute control using spline interpolation
u = z(params.u);
x1_states = X(:,1);
x2_states = X(:,2);
%derivatives
dxdt(:,1) = x2_states;
dxdt(:,2) = mu.*(1-x1_states.^2).*x2_states-x1_states+u;

end