function [] = PhasePlane(f,ub,lb,initCond)


%set number of vectors to plot and differential system
N = 50;
x_dot = f;

%Phase plane 1a
x1 = linspace(-ub,lb,N);
x2 = linspace(-ub,lb,N);
[x,y] = meshgrid(x1,x2);

%compute vector field
u = zeros(size(x));
v = zeros(size(x));

for i = 1:numel(x)
    dxdt = x_dot(0,[x(i),y(i)]);
    u(i) = dxdt(1);
    v(i) = dxdt(2);
end

%normalize
% u = u./sqrt(u.^2+v.^2);
% v = v./sqrt(u.^2+v.^2);

%create plot via quiver()
figure
quiver(x,y,u,v,2)
xlabel('x_1')
ylabel('x_2')
name2{1} = ['Vector Field'];

%initial conditions to simulate
ICs = initCond;
tspan = [0 100];

for i = 1:size(ICs,1)
    %current IC
    x0 = ICs(i,:);
    [t,z] = ode45(@(t,x) x_dot(t,x),tspan,x0);
    hold on
    plot(z(:,1),z(:,2))
    name2{i+1} = ['IC: ' num2str(ICs(i,:))];
end

%adjust figure limits
xlim([min(x1) max(x1)])
ylim([min(x2) max(x2)])
title('Phase Plane')
legend(name2)
end

