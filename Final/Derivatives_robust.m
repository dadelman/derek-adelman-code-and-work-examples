function dxdt = Derivatives_robust(t,x,controls,PolyModel_params,B,params)

% compute control using spline interpolation
u = interp1(params.t,controls,t,'spline');

%derivatives
dxdt(1) = x(2);
dxdt(2) = PolyModel_params(1,B)*(1-x(1)^2)*x(2)-x(1)+u;
dxdt = dxdt.';

end