function dxdt = Derivatives(t,x,controls,params)

% compute control using spline interpolation
u = interp1(params.t,controls,t,'spline');

%derivatives
dxdt(1) = x(2);
dxdt(2) = params.mu*(1-x(1)^2)*x(2)-x(1)+u;
dxdt = dxdt.';

end