function [u] = OptControlInput(t,x,Time,R,B,N,P)

P_interp = interp1(Time,P,t,'spline');
P_interp_matrix = [P_interp(1) P_interp(2);P_interp(3) P_interp(4)];
K = R^-1*(N.'+B.'*P_interp_matrix);
u = -K*x;
if(t>50e-3 && max(Time)<100e-3)
    P_interp_matrix = [0 0;0 0];
    K = R^-1*(N.'+B.'*P_interp_matrix);
    u = -K*x;
end

end

