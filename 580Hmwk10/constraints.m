function [g,h] = constraints(z,params)

% extract controls from Z
u = z(params.MineRate_i);

% simulate
X = runSimulation(u,params);

% boundary (final state) constraints
%none

% path constraints
g1 = 0-X;
g2 = X-params.x0;
% combine constraints
g = [g1;g2];
h = [];


end

