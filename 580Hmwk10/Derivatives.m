function dxdt = Derivatives(t,x,controls,params)

% compute control using spline interpolation
u = interp1(params.t,controls,t,'spline');

%derivatives
dxdt = -u;

end

