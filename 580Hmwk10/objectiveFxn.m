function J = objectiveFxn(z,params)

%extract indexed control points
controls = z(params.MineRate_i);

X = runSimulation(controls,params);

%evaluate the objective function
LagrangeTerm = trapz(params.t,z.^2./X-params.a*z);

MayerTerm = 0;

J = LagrangeTerm + MayerTerm;

end

