function [dpdt] = Prob1di(t,p,A,B,N,R,Q)

%construct P
P = [p(1) p(3);p(2) p(4)];

%diff equns
dpdt = -(Q+A.'*P+P*A-(P*B+N)*R^-1*(N.'+B.'*P));

%collect derives in column vector
dpdt = [dpdt(1,1) dpdt(2,1) dpdt(1,2) dpdt(2,2)].';

end

