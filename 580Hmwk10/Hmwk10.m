%% Derek Adelman SYSE580 Hmwk 10
%%
clear 
clc
format compact
close all
%% Problem 1
%% a
%note alpha must be positive if it is a true damping coefficient
syms alpha beta p11 p12 p21 p22 rho

%define system matrices
A = [0 1;0 -alpha];
B = [0;beta];
Q = [1 0;0 0]
R = rho;
N = 0
P = [p11 p12;p21 p22];

%algebraic ricatti equations
ARE = Q+A.'*P+P*A-(P*B+N)*(R^-1)*(N.'+B.'*P);

%solve ARE
P_inf = solve([0==ARE],P);

%determine which solution to ARE is the pos semi def one
for i = 1:6
    
    %construct ith solution to P from solve() output
    P_soln{i} = [simplify(P_inf.p11(i,:)) simplify(P_inf.p12(i,:)); simplify(P_inf.p21(i,:)) simplify(P_inf.p22(i,:))];
    
    %check if matrix is symmetrical
    if(P_soln{i}(1,2)~=P_soln{i}(2,1))
        P_soln{i} = 0;
    else
        %if it is, find eigvals
        EigVals{i} = simplify(eig(P_soln{i}));
        
        %to find which is the correct form, we know there is only one positive
        %semidefinite solution. So if we check the eigenvalues, then any
        %that have nonpositive eigvals can be eliminated as candidate
        %solutions. Use alpha and beta as positive as these make physical
        %sense with the problem. Rho is defined >0

        %find numeric eigval
        Num_EigVal(:,i) = double(subs(EigVals{i},[sym('alpha') sym('beta') sym('rho')],[2*1e6*rand(1) 2*1e6*rand(1) 2e6*rand(1)]));
    
    end
    
    %check if any eigvals are less than zero. Does not apply to solutions
    %that are not symmetric (set as {0} above)
    try
        if(all(Num_EigVal(:,i)>=0))
            SolutionIndex = i
        end
    catch
        disp(['Solution for index = ' num2str(i) ' does not apply because P is not symmetric.'])
    end
    
end

%this method worked for this problem. Not robust to finding the one pos
%semi-def solution to more complex problems
Num_EigVal
P_inf_unique_solution = P_soln{SolutionIndex}

%optimal control gain
K_inf = R^-1*(N.'+B.'*P_inf_unique_solution)
K_inf_numeric = double(subs(K_inf,[sym('alpha') sym('beta') sym('rho')],[7.14 286.3 1]))
%% b

%OL poles
EigVal_OL_fxn = matlabFunction(eig(A))

%CL system
A_cl = simplify(A-B*K_inf)
EigVal_CL = eig(A_cl)
EigVal_CL_fxn = matlabFunction(EigVal_CL)

%values to try for rho
p = linspace(.01,250,1000).';
fig = figure;

%cases to plot
Cases = 3;
for i = 1:Cases
    
    %randomly choose alpha and beta >0
    a(i) = double(100*rand(1));
    b(i) = double(500*rand(1));
    
    %unforced poles
    UnforcedPoles(:,i) = EigVal_OL_fxn(a(i));
   
    %vary weight parameter
    for j = 1:length(p)
        EigVal_CL_array{i}(:,j) = EigVal_CL_fxn(a(i),b(i),p(j));
    end
    
    %plot results--only plot real part of eigenvalues
    figure(fig)
    subplot(Cases,1,i);hold on
    plot(p,EigVal_CL_array{i},'LineWidth',1)   
    yline(UnforcedPoles(1,i),'r-.','LineWidth',1);
    yline(UnforcedPoles(2,i),'k-.','LineWidth',1);
    title(['\alpha = ' num2str(a(i)) ' \beta = ' num2str(b(i))])
    legend('\lambda_1','\lambda_2','Unforced \lambda_1','Unforced \lambda_2')
    ylabel('Real(\lambda)')
    
end

%mark up figure
figure(fig)
subplot(Cases,1,i)
xlabel('\rho')
%% Comment
% Note how the real part of each eigenvalue aymptotically approaches the
% corresponding real part of the unforced original eigenvalue of the
% system. This seems to indicate that optimal LQR control is achieved by
% modifying the system such that it behaves as closely as possible to the
% uncontrolled system while maintaining controllability, resulting in
% "non-wasted" control effort. Depending on the original eigenvalues, it
% may take a relatively large value of rho before the forced system
% eigenvalues diverge to their respective unforced eigenvalues.
%% c
syms r x

%parameters
a = 7.14;
b = 286.3;

%closed loop system
A_cl = matlabFunction(A-B*K_inf);

%closed loop system function
CL_sys = @(r,rho,x) A_cl(a,b,rho)*x+[0;1]*r;

%initial condition (detemined through trial and error)
p = 1;

%init counter
i = 1;
fig1 = figure;

%init sim clock
tic
while 1
    
    %sim and sim parameters
    x0 = [0.3 0];
    tspan = [0 200e-3];
    opts = odeset('reltol',1e-10,'abstol',1e-10);
    [t,x] = ode45(@(t,x) CL_sys(0,p,x),tspan,x0,opts);
    
    %save results
    results{i}.t = t;
    results{i}.x = x;
    
    %calculate and save rise time
    Specs{i} = stepinfo(x(:,1),t);
    RiseTime(i) = Specs{i}.RiseTime;
    
    %check break condition
    SimTime = toc;
    if(Specs{i}.RiseTime<50e-3 || SimTime>120)
        rho_vector(i) = p;
        break;
    else
        rho_vector(i) = p;
        %decrease rho (direction determined via experimentation)
        p = p-.001;
    end

    %increase index
    i = i+1;
end

%report final rise time found
RiseTime(end)

%plot results
figure(fig1)
plot(results{i}.t,results{i}.x(:,1))
ylabel('Position \theta')
title(['\rho = ' num2str(p)])
xlabel('time-s')

figure
plot(rho_vector,RiseTime)
xlabel('\rho')
ylabel('Rise Time-s')

%compare to matlab constructed system
sys = ss(double(subs(A-B*K_inf,[sym('alpha') sym('beta') sym('rho')],[a b p])),[0;1],[1,0],0);
figure
step(sys)
stepinfo(sys)

%% d

%create numeric versions of system matrices
A_num = double(subs(A,[sym('alpha')],a));
B_num = double(subs(B,[sym('beta')],b));
R_num = double(subs(R,[sym('rho')],p));

%time horizons to simulate
TimeHorizon = [200e-3 50e-3];

for j = 1:length(TimeHorizon)

    %i
    
    %final condition (initial since simulating backwards)
    pf = zeros(2); %want the maximum final value of x to be zero (controlling to zero state)
    tspan = [TimeHorizon(j) 0];
    opts = odeset('reltol',1e-10,'abstol',1e-10);
    [t,P] = ode45(@(t,p) Prob1di(t,p,A_num,B_num,N,R_num,Q),tspan,pf,opts);

    %plot results
    figure
    plot(t,P)
    xlabel('time-s')
    ylabel('P(t)')
    title(['Time horizon = ' num2str(TimeHorizon(j)) ' s'])
    legend('P_{11}','P_{12}','P_{21}','P_{22}','location','best')

    %save time vector
    Time = t;

    %ii
    
    %sim and sim parameters
    x0 = [.3;0];
    tspan = [0 TimeHorizon(j)];
    opts = odeset('reltol',1e-10,'abstol',1e-10);
    [t,x] = ode45(@(t,x) Prob1dii(t,x,P,Time,A_num,B_num,R_num,N),tspan,x0,opts);
    
    %calculate u
    for i = 1:length(t)
        u(i) = OptControlInput(t(i),x(i,:).',Time,R_num,B_num,N,P);
    end

    %save results
    Results2d{j}.t = t;
    Results2d{j}.P = P;
    Results2d{j}.x = x;
    Results2d{j}.u = u;
    clear u
end

%plot results
figure
subplot(2,1,1); hold on
yyaxis left
plot(Results2d{1}.t,Results2d{1}.x(:,1))
plot(Results2d{2}.t,Results2d{2}.x(:,1))
ylabel('\theta')
title(['Dashed, Time Horizon = ' num2str(TimeHorizon(2)) ' Solid, Time Horizon = ' num2str(TimeHorizon(1))])
yyaxis right
plot(Results2d{1}.t,Results2d{1}.x(:,2))
plot(Results2d{2}.t,Results2d{2}.x(:,2))
ylabel('\omega')
subplot(2,1,2);hold on
plot(Results2d{1}.t,Results2d{1}.u)
plot(Results2d{2}.t,Results2d{2}.u)
xlabel('time-s')
ylabel('Optimal Control Input-V')
legend(['Time Horizon = ' num2str(TimeHorizon(1))],['Time Horizon = ' num2str(TimeHorizon(2))],'location','best')

%% Comment
% From the plot comparing the two simulation horizons, it is seen that the
% 50 ms time horizon simulation is not controlled to the zero state before
% the simulation time expires. This is because both time horizon
% simulations use the same weight value, rho, that generates a rise time of
% 50 ms. As such, the both solutions for P(t) use this value resulting in
% both optimal controls generating a rise time of 50 ms. Thus, if the rise
% time is 50 ms, a 50 ms simulation will not necessarily reach the
% steady-state controlled value. The system can be controlled to the zero
% state within the 50 ms time horizon by either reducing the rise time or
% increasing the weight matrix, Q. Since the value of P(0) = 0, the optimal
% control for both time horizons has a final boundary condition of 0, so u
% = 0 for both simulations at the final time. Also note that because the
% 50ms simulation ends while the system is still rising, the state
% experiences no overshoot. This is also seen in the control, which has no
% overshoot as well.

%% Problem 2

%% a

%parameters
params.a = 1;
params.x0 = 1;
params.tf = 1;

%shooting parameters
%number of tme points
params.numTime = 10;
params.t = linspace(0,params.tf,params.numTime).';
params.numControls = 1;

%discritize variable indices for control variable
params.MineRate_i = [1:params.numTime].';

%initial guess for control input, u (multiplication for the case in which
%there are multiple control values)
z0 = zeros(params.numTime*params.numControls,1);

% fmincon options
options = optimoptions(@fmincon,'display','iter','MaxFunEvals',10e3,...
    'UseParallel',true);

%optimize the control
z = fmincon(@(z) objectiveFxn(z,params),z0,[],[],[],[],[],[],@(z) constraints(z,params),options);

%extract control values from z
u = z(params.MineRate_i);

%simulate optimal soln on denser time grid
tspan = [0 params.tf];
opts = odeset('reltol',1e-13,'abstol',1e-13);
[t,x] = ode45(@(t,x) Derivatives(t,x,u,params),tspan,params.x0,opts);

%analytical optimal control/states
x_analytical = ((params.a*params.tf+4-params.a*t)./(params.a*params.tf+4)).^2*params.x0;
Costate_analytical = params.a^2*(params.tf-t)./(params.a*(params.tf-t)+4);
u_analytical = x_analytical.*(params.a-Costate_analytical)./2;

%plot control and states
figure
subplot(2,1,1);hold on
plot(t,x)
plot(t,x_analytical)
ylabel('state')
legend('x_{opt}','x_{analyticalOpt}')
subplot(2,1,2);hold on
plot(params.t,u,'o')
u_interp = interp1(params.t,u,t,'spline');
plot(t,u_interp)
plot(t,u_analytical)
xlabel('tims-s')
legend('Optimized control pt.','Interpolated optimal ctrl','Analytical opt ctrl')
ylabel('Control Input')

%%  b
% Looking at the above plot for the given problem parameters, only about 10
% points are needed for an accurate solution of the states, however the
% optimal control solution is relatively inaccurate for the first third of
% the simulation time. Doubling the number of evaluation points increases
% the optimal control accuracy (see "20optPoints" in folder), however a
% slight change in the problem parameters can drastically impact the
% accuracy of the numerical solution.

%% Functions
%
% <include>Prob1di.m</include>
%
% <include>Prob1dii.m</include>
%
% <include>objectiveFxn.m</include>
%
% <include>runSimulation.m</include>
%
% <include>constraints.m</include>
%
% <include>Derivatives.m</include>
%
% <include>OptControlInput.m</include>
