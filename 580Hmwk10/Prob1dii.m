function [dxdt] = Prob1dii(t,x,P,Time,A,B,R,N)

%interpolate P
P_interp = interp1(Time,P,t,'spline');

%construct P matrix
P_interp_matrix = [P_interp(1) P_interp(2);P_interp(3) P_interp(4)];

%calculate time varying K and u
K = R^-1*(N.'+B.'*P_interp_matrix);
u = -K*x;
% if(t>50e-3 && max(Time)<100e-3)
%     P_interp_matrix = [ 0 0;0 0];
%     K = R^-1*(N.'+B.'*P_interp_matrix);
%     u = -K*x;
% end

%system equations
dxdt = A*x+B*u;

end

