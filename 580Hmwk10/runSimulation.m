function SimulationOutput = runSimulation(controls,params)

%simulation options
opts = odeset('reltol',1e-13,'abstol',1e-13);

%run simulation, dont care about output time
[~,SimulationOutput] = ode45(@(t,x) Derivatives(t,x,controls,params),params.t,params.x0,opts);

end

