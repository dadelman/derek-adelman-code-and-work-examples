%% Encryption and Decryption Driver
%%
clear 
clc 
format compact
close all

alphabet = '.,?!"{}[]@#$%^&*()-=_+><\|~` abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

Message = 'Hybrid vehicles are generally defined as having two power sources that are capable of motoring the vehicle. The hybrid powertrain is the system of drive components (shafts, gears, engines, electric motors, etc.) that delivers the energy from the power sources to the wheels. Two primary powertrain configurations exist, the first a series hybrid, where only one power source is capable of directly driving the wheels. The most common application uses the electric motor (EM) to drive the wheels with the internal combustion engine (ICE) acting as a generator charging the high voltage battery pack. As such, the ICE cannot directly provide power to the wheels, however this allows the ICE to run in its most efficient operating range. The second is a parallel hybrid where both electric motor(s) and ICE can provide power to the wheels at the same time. Such configurations often use smaller electric motors and battery packs, using them in tandem with the ICE to increase fuel economy. There are five main parallel hybrid configurations, denoted P0 through P4 (parallel-0, parallel-1, etc.) with the number signifying the position of the electric motor with respect the ICE and transmission. P0 corresponds to placing the EM pre-ICE, P1 post-ICE and before any clutches connecting the ICE and transmission, P2 post-ICE and after any clutches, P3 after the transmission but before the propulsion shaft, and P4 generally replaces the differential or is located in the wheel hubs [49]. Note that P0 and P1 configurations can never be decoupled from the ICE whereas P2-P4 can via clutches or the transmission. For the TVP project, it is seen in [1] the architecture selected was a P3-parallel hybrid. This was largely due to the relative simplicity in construction and control required. '%Key must be at least length(alphabet)^(1/3) (eg 5 characters for 93 char alphabet) and no repeating
%characters
Key = 'fives'  %Next step is to figure out how to allow shorter keys with repeating chars and to not make
%encryption length a multiple of 3

Encryption = encrypt(Message,Key,alphabet)
length(Encryption)

Decryption = decrypt(Encryption,Key,alphabet)

%% Functions
% <include>encrypt.m</include>
%
% <include>decrypt.m</include>
