function [encryption] = encrypt(Message,Key,alphabet)

%convert each symbol in message to its corresponding numerical position in
%alphabet
for i = 1:numel(Message)
    for j = 1:numel(alphabet)
        if(Message(i)==alphabet(j))
        Num(i) = j;
        end
    end
end
%matrix that determines where message letters are placed in encoding matrix
%  K e y w o r d
%K 1 8 15
%e 2 9 .
%y 3 10 .
%w 4 11 .
%o 5 12
%r 6 13
%d 7 14

%note that the size of the encoding matrix determined by KEYWORD needs to
%be able to hold all the symbols in ALPHABET
for L = 1:length(Key)
    for r = 1:length(Key)
        for c = 1:length(Key)
            LettoNumMat(r,c,L) = r+(c-1)*length(Key)+(L-1)*length(Key)^2;
        end
    end
end

% for r = 1:length(Key)
%         for c = 1:length(Key)
%             LettoNumMat(r,c) = r+(c-1)*length(Key);
%         end
%     end

%encryption matrix-find where alphabet number corresponds to encoding
%matrix number and then convert to letters via encoding matrix r,c,L letter
%assignments
for j = 1:length(Num)
    [h,g] = find(LettoNumMat==Num(j));
    [row column layer] = ind2sub(size(LettoNumMat),Num(j));
    EM1(j,:) = [Key(row) Key(column) Key(layer)];
    for z = 1:3
        loc(j,z) = find(alphabet==EM1(j,z));
    end
end
loc;
loc = vertcat(loc(:,1),loc(:,2),loc(:,3));
for k = 1:length(loc)
     if(rem(k,2)==0)
         EM2(k,1) = loc(k)+k;
     else
         EM2(k,1) = loc(k)+3*k;
     end
end
EM3 = rem(EM2,length(alphabet)) + 1; %THIS IS THE KEY LINE. HAVE TO SHIFT BACK IN ALPHABET SO NO ZEROS OCCUR
           
%convert EM2 to str of letters
EM3;

N2L(:) = alphabet(EM3(:));
encryption = N2L;
end

