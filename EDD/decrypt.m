function [decryption] = decrypt(Message,Key,alphabet)

for L = 1:length(Key)
    for r = 1:length(Key)
        for c = 1:length(Key)
            LettoNumMat(r,c,L) = r+(c-1)*length(Key)+(L-1)*length(Key)^2;
        end
    end
end

for i = 1:length(Message)
    EM3(i,1) = find(Message(i)==alphabet);
end
EM3 = EM3-1;
for k = 1:length(Message)
     if(rem(k,2)==0)
         EM2(k,1) = EM3(k)-k;
     else
         EM2(k,1) = EM3(k)-3*k;
     end
end

EM2 = rem(EM2,length(alphabet));
EM2(EM2<0) = ones(length(EM2(EM2<0)),1)*length(alphabet)+EM2(EM2<0);
% EM2(EM2==0)=1;
loc = [EM2(1:length(EM2)/3) EM2(length(EM2)/3+1:2*length(EM2)/3) EM2(2*length(EM2)/3+1:end)];

for i = 1:length(loc)
    for z = 1:3
        EM1(i,z) = alphabet(loc(i,z));
    end
end

for i = 1:length(EM1)
    for z = 1:3   
            loc2(i,z) = find(Key==EM1(i,z));
        loc2;
    end
end

for i = 1:length(loc2)
    Num(i) = LettoNumMat(loc2(i,1),loc2(i,2),loc2(i,3));
end

for i = 1:length(Num)
        decryption(i) = alphabet(Num(i));
end

end

