function [dxdt] = Prob2d(t,x,A,B,C,D,L,x0_ss,u0_ss,mu,K)

%control input using obsv states
u = u0_ss-K*(x(3:4)-x0_ss);

%nonlinear system
dxdt(1:2) = [x(2);mu*(1-x(1)^2)*x(2)-x(1)+u];

%obsv states
dxdt(3:4) = L*C*(x(1:2)-x0_ss)+(A-L*C)*(x(3:4)-x0_ss)+B*(u);

%control input for extraction
dxdt(5) = u;
dxdt = dxdt.';

end

