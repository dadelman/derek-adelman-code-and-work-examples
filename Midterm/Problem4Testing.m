clear
clc
close all
format compact

%% Problem 4

%% a
syms x1 x2 x3 x4 g L M m u

%define state space system equations
f1 = x2;
f2 = (3*g/(4*L)*(M+m)*sin(x1)-3/4*m*x2^2*sin(x1)*cos(x1)-3/(4*L)*u*cos(x1))/(M+m-3/4*m*cos(x1)^2);
f3 = x4;
f4 = (x2^2*m*L*sin(x1)-3/4*m*g*sin(x1)*cos(x1)+u)/(M+m-3/4*m*cos(x1)^2);

%collect terms
f = [f1;f2;f3;f4];
x = [x1;x2;x3;x4];

%determine equlibrium position
f_0 = subs(f,[sym('x1') sym('x2') sym('x3') sym('x4')],[0 0 0 0])
u0_ss = solve(f_0==0,u)

%define ouput equations
g_0 = [x1];

%compute A, B, C, D matrices
A = double(subs(jacobian(f,x),[sym('x1') sym('x2') sym('x3') sym('x4') sym('M') sym('m') sym('L') sym('g')],[0 0 0 0 2 1 0.5 10]))
B = double(subs(jacobian(f,u),[sym('x1') sym('x2') sym('x3') sym('x4') sym('M') sym('m') sym('L') sym('g')],[0 0 0 0 2 1 0.5 10]))
C = double(jacobian(g_0,x))
D = 0;

IsControllable = isequal(rank(ctrb(A,B)),length(A))
IsObservable = isequal(rank(obsv(A,C)),length(A))
rank(obsv(A,C))
%since the system is not observable, Observer subspace decomposition can be
%employed to observe the observable states or a reduced order observer can
%possibly be constructed. Look into further

%% b

%stability via Indirect method
EigVals = eig(A)

syms lambda
%determine sys(i) eigenvalues
EigSys = solve(det(lambda*eye(size(A))-A)==0,lambda);
i=1
%eval stability based eigenvalues and output corresponding message
if(any(real(EigSys)>0))
%     F = [repmat(' %d',1,length(A)),'\n'];
%     fprintf('System %d,\n',i)
%     fprintf(F,A.')
    A = A
    fprintf('is unstable.\n')
elseif(all(real(EigSys)<0))
%     F = [repmat(' %d',1,length(A)),'\n'];
%     fprintf('System %d,\n',i)
%     fprintf(F,A.')
    A = A
    fprintf('is asymptotically stable.\n')
elseif(any(real(EigSys)==0))
%     F = [repmat(' %d',1,length(A)),'\n'];
%     fprintf('System %d,\n',i)
%     fprintf(F,A.')
    A = A
    fprintf('may be externally stable but internally unstable. See plots.\n')
else
%     F = [repmat(' %d',1,length(A)),'\n'];
%     fprintf('System %d,\n',i)
%     fprintf(F,A.')
    A = A
    fprintf('is stable in the sense of Lyapunov (marginaly stable).\n')
end

sysSim = @(t,x)A*x;
tspan = [0,3];
x0 = rand(length(A),1);
[t,x] = ode45(@(t,x) sysSim(t,x),tspan,x0);
figure
plot(t,x)
title(['System A'])

%% c

OL_sys = ss(A,B,C,D)
P_controller = pidtune(OL_sys,'P')
CL_sys = feedback(P_controller*OL_sys,1)
figure
step(CL_sys,2)
stepinfo(CL_sys)

%extract Kp
K = get(P_controller,'Kp')
r = 0;
x0_ss = [0;0;0;0]
x0 = [0; 0.5; 0; 0;0;0.5;0;0];
tspan = [0 50];
opts = odeset('reltol',1e-10,'abstol',1e-10);
[t,x] = ode45(@(t,x) Prob3c(t,x,A,B,C,D,K,r,x0_ss,u0_ss),tspan,x0,opts);

for i = 1:length(x(:,1))
    y(i) = C*x(i,1:4).';
end

figure;hold on
plot(t,y)
plot(t,x(:,5))
legend('linear','nonlinear')


% sys = ss(A,B,C,D)
% C = pidtune(sys,'P')
% CL_sys = feedback(sys,C)
% t = 0:.01:50;
% [y,t,x] = lsim(CL_sys,zeros(size(t)),t,[0 0.5 0 0]);
% figure
% plot(t,x)
% 
% syms x1 x2 x3 x4 u
% solve(f==0,u)

%% d

OL_sys = ss(A,B,C,D)
PI_controller = pidtune(OL_sys,'PI')
CL_sys = feedback(OL_sys*PI_controller,1)
figure
step(CL_sys)
stepinfo(CL_sys)

K = -10
Ki = 1
r = 0;
x0 = [0; 0.5; 0; 0;0;0.5;0;0;0;0];
tspan = [0 50];
opts = odeset('reltol',1e-10,'abstol',1e-10);
[t,x] = ode45(@(t,x) Prob3d(t,x,A,B,C,D,K,r,x0_ss,u0_ss,Ki),tspan,x0,opts);

figure;hold on
plot(t,x(:,1))
plot(t,x(:,5))
legend('linear','nonlinear')


M = 2;
m = 1;
g = 10;
L = 0.5;
%% e
% clearvars -except A B C D x0_ss u0_ss
states = {'theta' 'thetadot' 'p' 'pdot'};
inputs = {'force'};
outputs = {'y'};

OL_sys = ss(A,B,C,D,'statename',states,...
'inputname',inputs,...
'outputname',outputs);
% OL_sys = ss(A,B,C,D)
PD_controller = pidtune(OL_sys,'PD')
CL_sys = feedback(OL_sys*PD_controller,1);
% states = {'theta' 'thetadot' 'p' 'pdot' 'pd1'};
% inputs = {'force'};
% outputs = {'y'};
% CL_sys = set(CL_sys,'statename',states,...
% 'inputname',inputs,...
% 'outputname',outputs)
figure
step(CL_sys)
stepinfo(CL_sys)
[Isprop,sysr] = isproper(CL_sys)
% [b,a] = ss2tf(CL_sys.A,CL_sys.B,CL_sys.C,CL_sys.D,CL_sys.E)
E_cl = CL_sys.E;
A_cl = CL_sys.A;
B_cl = CL_sys.B;
C_cl = CL_sys.C;
D_cl = CL_sys.D;


% figure
% bode(CL_sys,sysr,'r--')
% 
global errorPast
global tPast
global FcnCall
FcnCall = 0
errorPast = double.empty;
tPast = double.empty;
K = get(PD_controller,'Kp')
Kd = get(PD_controller,'Kd')
r = 0;
x0 = [0; 0.5; 0; 0;0;0.5;0;0];
tspan = [0 50];
opts = odeset('reltol',1e-10,'abstol',1e-10);
[t,x] = ode45(@(t,x) Prob3e(t,x,A,B,C,D,K,Kd,r),tspan,x0,opts);

figure; hold on
plot(t,x(:,1))
plot(t,x(:,5))
% 

% OL_sys = tf(OL_sys);
PDF_controller = pidtune(OL_sys,'PDF')

CL_sys = feedback(OL_sys*PDF_controller,1);

t = 0:.01:50;
[y,t,x] = lsim(CL_sys,zeros(size(t)),t,[0 0.5 0 0 0]);
figure
plot(t,y)
title('lsim')

