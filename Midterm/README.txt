Folder Notes:
-580MidtermHandwork.pdf contains handwork for problems 1 and 3
-Midterm.m is the code that runs all problems. Problems can be run individually by Ctrl+ENTER when cursor is in desired section.
-html folder contains html output that is the easiest way to see all code and associated outputs. The publish feature for some reason would sometimes duplicate figures. Not sure why, but all info is there.
-Prob4Testing.slx is the Simulink model that is compared to matlab methods in problem 4