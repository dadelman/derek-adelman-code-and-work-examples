function [dxdt] = Prob3c(t,x,A,B,C,D,K,r,x0_ss,u0_ss,Ki)
%LINEAR PLANT
%linear control input
u_lin = K*(r-C*x(1:4))+Ki*x(9);

%linear system
dxdt(1:4) = A*x(1:4)+B*u_lin;

%NONLINEAR PLANT
%constants
M = 2;
m = 1;
g = 10;
L = 0.5;

%same input as linear system, but uses nonlinear state values
u_nonlin = K*(r-C*x(5:8))+Ki*x(10);

%nonlinear system
dxdt(5:8) = [x(6);...
    -((3*m*cos(x(5))*sin(x(5))*x(6)^2)/4 + (3*u_nonlin*cos(x(5)))/(4*L) - (3*g*sin(x(5))*(M + m))/(4*L))/(M + m - (3*m*cos(x(5))^2)/4);...
    x(8);...
    (L*m*sin(x(5))*x(6)^2 + u_nonlin - (3*g*m*cos(x(5))*sin(x(5)))/4)/(M + m - (3*m*cos(x(5))^2)/4)];

dxdt = dxdt.';

%integral states for linear and nonlinear systems
dxdt(9) = r-C*x(1:4);
dxdt(10) = r-C*x(5:8);
end

