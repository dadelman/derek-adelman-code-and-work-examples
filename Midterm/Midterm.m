%% Derek Adelman SYSE580 Midterm
%%
clear
clc
close all
format compact

%% Problem 1

%% i

%determine eigenvals
A = [0 0 0;1 0 -20;0 1 -9];
EigVals = eig(A)

%check if matrix exponential to make sure it has all exp(-t) terms
syms t x10 x20 x30; 
eA = expm(A*t)*[x10;x20;x30]

%simulate states
x_dot = @(t,x) A*x;
x0 = -10 + (10 + 10)*rand(3,1);
tspan = [0 10];
opts = odeset('reltol',1e-10,'abstol',1e-10);
[t,x] = ode45(@(t,x) x_dot(t,x),tspan,x0,opts);

%plot
figure
plot(t,x)
xlabel('time-s')
ylabel('state-x')
legend('x1','x2','x3')

%% ii

%compute eigenvals
A = [-4 -6 10;6 4 -10;4 2 -6];
EigVals = eig(A)

%check that matrix exponential has all exp(-t) terms (it does not)
syms t x10 x20 x30; 
eA = expm(A*t)*[x10;x20;x30]

%simulate states
x_dot = @(t,x) A*x;
x0 = -10 + (10 + 10)*rand(3,1);
tspan = [0 10];
opts = odeset('reltol',1e-10,'abstol',1e-10);
[t,x] = ode45(@(t,x) x_dot(t,x),tspan,x0,opts);

%plot
figure
plot(t,x)
xlabel('time-s')
ylabel('state-x')
legend('x1','x2','x3')

%% iii

%check that V is indeed positive definite
[X,Y] = meshgrid(-1:0.01:1,-1:.01:1);
Z = 2*X.^2+2*X.*Y+Y.^2;

%plot V(x)
figure
s = surf(X,Y,Z);
s.EdgeColor = 'none';

%double check that 0 is the minimum and that V is not psd
X = @(x) 2*x(1)^2+2*x(1)*x(2)+x(2)^2;
options = optimset('TolFun',1e-10,'TolX',1e-10);
[x,fval,exitflag,output] = fminsearch(X,[1,1],options)
syms x1 x2
sln = solve([2*x2+2*x2;-4*x1-2*x2]==0,[x1 x2]);
sln.x1
sln.x2

%find derivative of V
syms x1 x2 t
x_dot1 = 2*x1+2*x2
x_dot2 = -4*x1-2*x2
V_dot = 4*x1*x_dot1+2*x2*x_dot1+2*x1*x_dot2+2*x2*x_dot2
SimpleV_dot = simplify(V_dot)

%plot phase plane
x_dot = @(t,x) [2*x(1)+2*x(2);-4*x(1)-2*x(2)];
PhasePlane(x_dot,10,10,[-10+20*rand(1,2);-10+20*rand(1,2);-10+20*rand(1,2)])

%% iv

%create system
syms x1(t) x2(t)
x_dot1 = x2
x_dot2 = -x2-(2+sin(t))*x1

%find V_dot and simplify handwork
V = x1^2+1/(2+sin(t))*x2^2;
V_dot = diff(V,t)
pretty(simplify(V_dot,'Steps',100))
V_dot = 2*x1*x_dot1+-cos(t)/(sin(t)+2)^2*x2^2+2*x2*x_dot2*1/(2+sin(t))
pretty(simplify(V_dot,'Steps',100))

%create phase plane
x_dot = @(t,x) [x(2);-x(2)-(2+sin(t))*x(1)];
PhasePlane(x_dot,5,5,[-5+10*rand(1,2);-5+10*rand(1,2);-5+10*rand(1,2)])

%% Problem 2
% Note some plots of sims have been zoomed in to show detail. All sims are
% run for at least 50s.

%% a

syms x1 x2 u

%create system
mu  = 1.5;
x = [x1;x2];
f = [x2;mu*(1-x1^2)*x2-x1+u];
g = x1;

%solve for x2 and u at the steady state given for x1
SteadyState = solve(subs(f,sym('x1'),0.5)==0,[u x2]);
u_ss = SteadyState.u
x2_ss = SteadyState.x2

%create system matrices
A = jacobian(f,x);
B = jacobian(f,u);
C = jacobian(g,x);
D = jacobian(g,u);

%SS equlibrium point
EqPts = solve(f==0,[x2,u]);

%eval system matrics at the equlibrium point
A = double(subs(A,[sym('x1') sym('x2') sym('u')],[0.5 SteadyState.x2 SteadyState.u]))
B = double(subs(B,[sym('x1') sym('x2') sym('u')],[0.5 SteadyState.x2 SteadyState.u]))
C = double(subs(C,[sym('x1') sym('x2') sym('u')],[0.5 SteadyState.x2 SteadyState.u]))
D = double(subs(D,[sym('x1') sym('x2') sym('u')],[0.5 SteadyState.x2 SteadyState.u]))

%determine controllablity and obsv
IsControllable = isequal(rank(ctrb(A,B)),length(A))
IsObservable = isequal(rank(obsv(A,C)),length(A))

%% b

%Re(lambda) to sim for L gains
alpha = [1 10 100];

for j = 1:length(alpha)
    
    %compute obsv gains
    L = place(double(A.'),double(C.'),[-alpha(j)+.5i,-alpha(j)-.5i]).'

    %define SS x and u
    x0_ss = double([0.5; SteadyState.x2]);
    u0_ss = double(SteadyState.u);

    %system sim function
    x_dot_nonlinear = @(t,x) [x(2);mu*(1-x(1)^2)*x(2)-x(1)+0.7];

    %simulate
    tspan = [0 50];
    x0 = [.5;0;-.5;0];
    opts = odeset('reltol',1e-10,'abstol',1e-10);
    [t,x] = ode45(@(t,x) Prob2b(t,x,A,B,C,D,L,x0_ss,u0_ss,mu),tspan,x0,opts);

    %plot results
    figure
    plot(t,x)
    legend('x1','x2','x1_{est}','x2_{est}')
    title(['alpha = ' num2str(alpha(j))])
    xlim([0 20])
    ylim([-4 6])
    xlabel('time-s')
    ylabel('states')
end

% alpha = 100 results in good tracking performance of the actual states

%% c

%Re(lambda) to sim for K gains
alpha = [1 0.1 0.01];

for i = 1:length(alpha)
    
    %compute feedback gains for system
    K = place(A,B,[-alpha(i)-.5i -alpha(i)+.5i])
    
    %simulate
    tspan = [0 50];
    x0 = [0.6;-.2;0]; %third state is for control input
    opts = odeset('reltol',1e-10,'abstol',1e-10);
    [t,x] = ode45(@(t,x) Prob2c(t,x,A,B,C,D,L,x0_ss,u0_ss,mu,K),tspan,x0,opts);
    
    %save results
    results{i}.t = t;
    results{i}.x = x(:,1:2);
    results{i}.u = x(:,3)
    
    %plot results
    fig{i} = figure;
    figure(fig{i}); hold on
    yyaxis left
    plot(results{i}.t,results{i}.x)
    title(['alpha = ' num2str(alpha(i))])
    xlabel('time-s')
    ylabel('states')
    yyaxis right
    plot(results{i}.t,gradient(results{i}.u)./gradient(results{i}.t))
    ylabel('control input')
    legend('x1','x2','control input')
    
end

%% d

%Re(lambda) to sim for K gains
alpha = [1 0.1 0.01];

for i = 1:length(alpha)
    
    %compute K gains
    K = place(A,B,[-alpha(i)-.5i -alpha(i)+.5i]);
    
    %simulate both obsv and state feedback
    tspan = [0 50];
    x0 = [0.6;-.2;-0.5;0;0]; %5th state is for control input
    opts = odeset('reltol',1e-10,'abstol',1e-10);
    [t,x] = ode45(@(t,x) Prob2d(t,x,A,B,C,D,L,x0_ss,u0_ss,mu,K),tspan,x0,opts);
    
    %save results
    results{i}.t = t;
    results{i}.x = x(:,1:4);
    results{i}.u = x(:,5);
    
    %plot results
    fig{i} = figure;
    figure(fig{i})
    subplot(2,1,1)
    plot(results{i}.t,results{i}.x)
    legend('x1','x2','x1_{est}','x2_{est}')
    ylabel('states')
    ylim([-2 1.25])
    title(['alpha_K = ' num2str(alpha(i)) ', alpha_L = 100'])
    subplot(2,1,2)
    plot(results{i}.t,gradient(results{i}.u)./gradient(results{i}.t))
    ylabel('control input')
    xlabel('time-s')
    ylim([-2 2])
    
end

%% Comment
% Similarly to part C, the increasing values of alpha result in less
% oscillation and convergence. For alpha = 0.01, the oscillations never
% fully dissipate (take simulation out to 1000s), whereas with alpha = 1
% the system reaches steady-state in roughly 10 seconds. For an initial
% condition of [-0.5;0], the estimated states converge very quickly (less
% than 0.5 sec) with eigenvalues for L at [-100+.5i,-100-.5i]. However,
% both estimated states experience steady-state error relative to the true
% states, x1 and x2. This can be reduced by moving the eigenvalues further
% to the left or eliminated by adding an integrator to the full-order
% observer.

%% Problem 3
% See handwork

%% Problem 4
% Note that x(t) = x0 + delta_x. Since the system was linearized around the
% equlibrium point x0 = 0, then x(t) = delta_x. It is shown for x0 = 0
% that u0 = 0. So the linearized system A*delta_x + B*delta_u can be
% simulated as A*x(t) + B*u(t) close to the origin.

%% a
syms x1 x2 x3 x4 g L M m u

%define state space system equations
f1 = x2;
f2 = (3*g/(4*L)*(M+m)*sin(x1)-3/4*m*x2^2*sin(x1)*cos(x1)-3/(4*L)*u*cos(x1))/(M+m-3/4*m*cos(x1)^2);
f3 = x4;
f4 = (x2^2*m*L*sin(x1)-3/4*m*g*sin(x1)*cos(x1)+u)/(M+m-3/4*m*cos(x1)^2);
f = [f1;f2;f3;f4]

%determine control input equlibrium position
f_0 = subs(f,[sym('x1') sym('x2') sym('x3') sym('x4')],[0 0 0 0])
u0_ss = solve(f_0==0,u)

%define ouput equations
g_0 = [x1];

%collect terms
x = [x1;x2;x3;x4];

%compute A, B, C, D matrices (same as what is given) at x = [0,0,0,0]
A = double(subs(jacobian(f,x),[sym('x1') sym('x2') sym('x3') sym('x4') sym('M') sym('m') sym('L') sym('g')],[0 0 0 0 2 1 0.5 10]))
B = double(subs(jacobian(f,u),[sym('x1') sym('x2') sym('x3') sym('x4') sym('M') sym('m') sym('L') sym('g')],[0 0 0 0 2 1 0.5 10]))
C = double(jacobian(g_0,x))
D = 0;

%determine controllability and observability
IsControllable = isequal(rank(ctrb(A,B)),length(A))
IsObservable = isequal(rank(obsv(A,C)),length(A))

%check rank of obsv matrix
ObsvMatRank = rank(obsv(A,C))

%obsv matrix
ObsvMat = obsv(A,C)

%Observer canonical form
canonSys = canon(ss(A,B,C,D),'companion')

%similarity transformation matrix for obsv decomp
P = [ObsvMat(1,:);ObsvMat(2,:);0 0 1 0; 0 0 0 1]

%subspace decomp
A_t = P*A*P^-1
B_t = P*B
C_t = C*P^-1

%subspace decomp observable system
Ao = A_t(1:2,1:2)
Bo = B_t(1:2)
Co = C_t(1:2)

%there are two observable states and two unobservable
eig(A)
eig(Ao)
%% Comment
% The system is not observable. Direct, full state feedback can be used to stablize
% the system. However, since the system is not observable full estimated
% state feedback is not possible (ie full state feedback via observers is
% not possible). Referencing the observable subspace decomposition section
% above, it is seen that only two states are observable. The unobservable
% states have eigenvalues [0;0] so these states are stable isL and the
% system is detectable. Thus, since the system is controllable and
% detectable, this system can be stabilized with state feedback of the
% unobservable states and estimated state feedback of the observable
% states.

%% b

%stability via Indirect method--if the linearized system is unstable at the
%origin the nonlinear system will be unstable near the origin as well
EigVals = eig(A)

syms lambda

%determine sys(i) eigenvalues
EigSys = solve(det(lambda*eye(size(A))-A)==0,lambda);

%eval stability based eigenvalues and output corresponding message
if(any(real(EigSys)>0))
    A = A
    fprintf('linear and nonlinear system is unstable.\n')
elseif(all(real(EigSys)<0))
    A = A
    fprintf('linear and nonlinear system is asymptotically stable.\n')
elseif(any(real(EigSys)==0))
    A = A
    fprintf('linear and nonlinear system may be externally stable but internally unstable. See plots.\n')
else
    A = A
    fprintf('linear and nonlinear system is stable in the sense of Lyapunov (marginaly stable).\n')
end

%simulate linearized system states and nonlinear states
M = 2;
m = 1;
g = 10;
L = 0.5;
u=0;
sysSim = @(t,x) [A*x(1:4);
                    x(6);
                    (3*g/(4*L)*(M+m)*sin(x(5))-3/4*m*x(6)^2*sin(x(5))*cos(x(5))-3/(4*L)*u*cos(x(5)))/(M+m-3/4*m*cos(x(5))^2);
                    x(8);
                    (x(6)^2*m*L*sin(x(5))-3/4*m*g*sin(x(5))*cos(x(5))+u)/(M+m-3/4*m*cos(x(5))^2)];
tspan = [0,30];
x0 = -.01+.02*rand(2*length(A),1);
[t,x] = ode45(@(t,x) sysSim(t,x),tspan,x0);

%plot
figure
subplot(2,1,1)
plot(t,x(:,1:4))
title('System A-linear')
ylabel('output-states x1-x4')
legend('x1','x2','x3','x4')
subplot(2,1,2)
plot(t,x(:,5:8))
title('System A-nonlinear')
xlabel('time-s')
ylabel('output-states x1-x4')
legend('x1','x2','x3','x4')


%% c
% Note that some sim plots have been zoomed in to show detail discussed in
% comment sections. All sims run for 50s.

%init values for Simulink file Prob4Testing.slx
K = 1;
Ki = 1;
Kd = 1;
N = 1;
M = 2;
m = 1;
g = 10;
L = 0.5;

open Prob4Testing
%% Matlab via pidtune

%linear open loop system
states = {'theta' 'thetadot' 'p' 'pdot'};
inputs = {'force'};
outputs = {'y'};

OL_sys = ss(A,B,C,D,'statename',states,...
'inputname',inputs,...
'outputname',outputs);

%create P_controller
P_controller = pidtune(OL_sys,'P')

%create closed loop system
CL_sys = feedback(P_controller*OL_sys,1);

%plot step response and step info
figure;hold on
[y,t,x] = step(CL_sys,3);
stepinfo(CL_sys)
plot(t,y)
yline(mean([max(y) min(y)]),'--');
title('P Step Response')
legend('Step Response','Oscillatory average')

%not all or arguably all time domain specs are applicable as there is not steady-state
%response--system is indefinitely oscillatory

%compute Time domain specs that make sense for this system (rise time for
%first oscillation, Overshoot (100% since oscillatory), Peak, and peak
%time)

tol = .1;
RiseTime = t(find(tol>=abs(y-.9*mean([max(y) min(y)])),1,'first'))-t(find(tol>=abs(y-.1*mean([max(y) min(y)])),1,'first'))

% SettlingTime = Not applicable

Peak = max(y)

Overshoot = (Peak-mean([max(y) min(y)]))/mean([max(y) min(y)])*100 % percentage

% Undershoot = not applicable

PeakTime = t(find(y==Peak,1,'first'))

%extract Kp
K = get(P_controller,'Kp')

%reference
r = 0;

%steady-state linearization point
x0_ss = [0;0;0;0]

%initial conditions for linear and nonlinear models
x0 = [0; 0.5; 0; 0;0;0.5;0;0];

%sim parameters and sim
tspan = [0 50];
opts = odeset('reltol',1e-10,'abstol',1e-10);
[t,x] = ode45(@(t,x) Prob3c(t,x,A,B,C,D,K,r,x0_ss,u0_ss),tspan,x0,opts);

%plot results note that y = x1
figure;hold on
plot(t,x(:,1:4))
ylim([-5 5])
legend('linear-x1','linear-x2','linear-x3','linear-x4')
title('4c-Matlab Linear')
xlabel('time-s')
ylabel('linear states')

figure; hold on
plot(t,x(:,5:8))
ylim([-5 5])
xlabel('time-s')
ylabel('nonlinear states')
legend('nonlinear-x1','nonlinear-x2','nonlinear-x3','nonlinear-x4')
title('4c-Matlab Nonlinear')


%% Simulink

%set linear parameters
set_param('Prob4Testing/LinCtrl','Controller','P')
set_param('Prob4Testing/LinCtrl','P','K')

%set nonlinear parameters
set_param('Prob4Testing/NonlinCtrl','Controller','P')
set_param('Prob4Testing/NonlinCtrl','P','K')

%simulate model
Results = sim('Prob4Testing');

%plot results
figure; hold on
plot(Results.tout,Results.LinSimModel.signals.values)
plot(Results.tout,Results.NonlinSimModel.signals.values)
legend('linear','nonlinear')
title('4c-Simulink')
xlabel('time-s')
ylabel('output-y(t)')

%% Comment
% From "4c-Matlab" it is seen that two states, both x3 in the linear and
% nonlinear models, are unstable. Note also that the output, x1, is stable
% isL. This shows that the linear model is externally stable but internally
% unstable under the control of a P-type controller. After tuning the
% controller, the best performance attainable is marginal stability,
% asymptotic stability cannot be achieved. Given that the output oscillates
% within about 0.1 radians of the origin this may be an acceptable control
% strategy. Note also that the output of the nonlinear model is nearly the
% same as the linear one in behavior and magnitude, however its frequency
% is slightly lower. Since the output of this system is marginally stable,
% time specifications do not really apply, however Rise Time, Peak,
% Overshoot, and Peak Time have been calculated for the first oscillation
% of the linear system. Matlab and Simulink results are in numerical
% agreement.
%% d

%% Matlab via pidtune

%linear model
states = {'theta' 'thetadot' 'p' 'pdot'};
inputs = {'force'};
outputs = {'y'};

OL_sys = ss(A,B,C,D,'statename',states,...
'inputname',inputs,...
'outputname',outputs);

%create PI controller
PI_controller = pidtune(OL_sys,'PI')

%create closed loop feedback system
CL_sys = feedback(PI_controller*OL_sys,1);

%plot step response (note system in unstable so time domain specs to do not
%apply)
figure
step(CL_sys,10)
stepinfo(CL_sys)
title('PI Step Response')

%extract Kp and Ki
K = get(PI_controller,'Kp')
Ki = get(PI_controller,'Ki')

%reference
r = 0;

%linear and nonlinear init states
x0 = [0; 0.5; 0; 0;0;0.5;0;0;0;0];

%params and sim
tspan = [0 50];
opts = odeset('reltol',1e-10,'abstol',1e-10);
[t,x] = ode45(@(t,x) Prob3d(t,x,A,B,C,D,K,r,x0_ss,u0_ss,Ki),tspan,x0,opts);

%plot
figure;hold on
plot(t,x(:,1:4))
legend('linear-x1','linear-x2','linear-x3','linear-x4')
title('4d-Matlab Linear')
xlabel('time-s')
ylabel('linear states')

figure; hold on
plot(t,x(:,5:8))
xlabel('time-s')
ylabel('nonlinear states')
legend('nonlinear-x1','nonlinear-x2','nonlinear-x3','nonlinear-x4')
title('4d-Matlab Nonlinear')

%% Simulink

%set linear parameters
set_param('Prob4Testing/LinCtrl','Controller','PI')
set_param('Prob4Testing/LinCtrl','P','K')
set_param('Prob4Testing/LinCtrl','I','Ki')

%set nonlinear parameters
set_param('Prob4Testing/NonlinCtrl','Controller','PI')
set_param('Prob4Testing/NonlinCtrl','P','K')
set_param('Prob4Testing/NonlinCtrl','I','Ki')

%simulate model
Results = sim('Prob4Testing');

%plot results
figure; hold on
plot(Results.tout,Results.LinSimModel.signals.values)
plot(Results.tout,Results.NonlinSimModel.signals.values)
legend('linear','nonlinear')
title('4d-Simulink')
xlabel('time-s')
ylabel('output-y(t)')

%% Comment
% Note first from "4d-Matlab" that both the linear and nonlinear systems
% are unstable in all states, except for x1 in the nonlinear state which
% appears to start converging to -5 around t = 25 sec. This corresponds
% with a rapid sudden divergence of the other 3 states. Also, the nonlinear
% states also diverge more quickly than the linear states. Asymptotic
% convergence to the origin is not possible with a PI controller for this
% system. Given that the system is unstable, time specifications are not
% applicable for the linear system. Since a P-type controller only
% marginally stabilized the system and that increasing Ki in general
% degrades system stability, it makes intuitive sense that a PI controller
% destabilizes the system and fails to control the output to the origin.
% Matlab and Simulink results are in numerical agreement for the output,
% x1.

%% e

%% Matlab via pidtune

%NOTE--linear without filter coefficient uses delta_e/delta_t to approximate
%derivative of error. Nonlinear uses x(2) for derivative
%linear open loop system
states = {'theta' 'thetadot' 'p' 'pdot'};
inputs = {'force'};
outputs = {'y'};

OL_sys = ss(A,B,C,D,'statename',states,...
'inputname',inputs,...
'outputname',outputs);

%compute PD controller
PD_controller = pidtune(OL_sys,'PD')
%create feedback with PD controller
CL_sys = feedback(PD_controller*OL_sys,1);
CL_sys1 = CL_sys;

PD_step = figure;

%create global error, time, and index vars so delta_e/delta_t can be
%implemented
global errorPast
global tPast
global FcnCall
FcnCall = 0;
errorPast = double.empty;
tPast = double.empty;

%extract Kp and Kd from PD controller
K = get(PD_controller,'Kp')
Kd = get(PD_controller,'Kd')

%simulate PD controller
r = 0;
x0 = [0; 0.5; 0; 0;0;0.5;0;0];
tspan = [0 50];
opts = odeset('reltol',1e-10,'abstol',1e-10);
[t,x] = ode45(@(t,x) Prob3e(t,x,A,B,C,D,K,Kd,r),tspan,x0,opts);

%plot results
figure; hold on
plot(t,x(:,1:4))
ylim([-2 2])
xlim([0 2])
legend('linear-x1','linear-x2','linear-x3','linear-x4')
title('4e-Matlab PD Linear')
xlabel('time-s')
ylabel('linear states')

figure; hold on
plot(t,x(:,5:8))
xlabel('time-s')
ylim([-2 2])
xlim([0 2])
ylabel('nonlinear states')
legend('nonlinear-x1','nonlinear-x2','nonlinear-x3','nonlinear-x4')
title('4e-Matlab PD Nonlinear')


%linear (use filter coefficient) PDF type--this makes the system explicit
%and easier to model with lsim() bc there is no E matrix with PDF type. PD type results
%in singular E matrix
%create PD controller with filter coefficient
PDF_controller = pidtune(OL_sys,'PDF')

%create closed loop system with PDF controller
CL_sys = feedback(OL_sys*PDF_controller,1);

%extract Kp Kd and filter coefficient
K = get(PDF_controller,'Kp')
Kd = get(PDF_controller,'Kd')
N = get(PDF_controller,'Tf')

%simulate via lsim
t = [0:.01:50].';
x0 = [0 0.5 0 0 0];
[Y,t,X] = lsim(CL_sys,zeros(size(t)),t,[0 0.5 0 0 0]);

%plot results
figure
plot(t,X(:,1:4))
ylim([-2 2])
xlim([0 2])
legend('linear-x1','linear-x2','linear-x3','linear-x4')
title('4e-Matlab PDF Linear')
xlabel('time-s')
ylabel('linear states')

%compare step plots and time domain specs of PD and PDF controllers
CL_sys2 = CL_sys;
figure(PD_step)
step(CL_sys1,CL_sys2)
StepInfoPD = stepinfo(CL_sys1)
StepInfoPDF = stepinfo(CL_sys2)
title('4e-PD & PDF Step Response')
legend('PD','PDF')



%% Simulink

%linear (must use PDF)
%set linear parameters
set_param('Prob4Testing/LinCtrl','Controller','PD')
set_param('Prob4Testing/LinCtrl','P','K')
set_param('Prob4Testing/LinCtrl','D','Kd')
set_param('Prob4Testing/LinCtrl','N','1/N')

%set linear parameters for manual PDF
set_param('Prob4Testing/KP','Gain','K')
set_param('Prob4Testing/KD','Gain','Kd')
set_param('Prob4Testing/Derivative','CoefficientInTFapproximation','1/N')

%set nonlinear parameters
set_param('Prob4Testing/NonlinCtrl','Controller','PD')
set_param('Prob4Testing/NonlinCtrl','P','K')
set_param('Prob4Testing/NonlinCtrl','D','Kd')
set_param('Prob4Testing/NonlinCtrl','N','1/N')

%simulate model
Results = sim('Prob4Testing');

%plot results
figure; hold on
plot(Results.tout,Results.LinSimModel.signals.values)
plot(Results.tout,Results.NonlinSimModel.signals.values)
plot(Results.tout,Results.LinManPD.signals.values)
xlim([0 1])
legend('linear-PDF','nonlinear-PDF','linear manual PDF')
title('4e-Simulink')
xlabel('time-s')
ylabel('output-y(t)')

bdclose('Prob4Testing')

%% Comment
% Firstly, since this system has an asymptotically stable output (however
% all systems still have an unstable x3 state), stepinfo() can be applied.
% Results are listed above for PD and PDF types. Now consider next that PD controllers can be
% implemented numerically in two primary ways, the first using
% delta_e/delta_t to approximate the derivative of error. This was used in
% the plots with "PD" in the title (or in the nonlinear case, x2 was used
% instead of delta_e/delta_t). The second uses a filter coefficient to
% approximate the transfer function Kd*s and this was used in plots with
% "PDF" in the title and in Simulink. It is seen in the PD, PDF, and step
% response plots that the output converges more quickly with less
% oscillatory behavior when the filter coefficient is applied. Note also
% the slight numerical differences in the Simulink plot between PDF
% controllers applied with the PD block and the "manual PDF" model (see
% Simulink). Looking between PD linear and nonlinear cases, it is seen that
% the state and output trajectories are nearly identical. The PDF for
% linear and nonlinear (compared to each other) are also practically
% identical and exhibit slightly lower magnitude than the PD cases. The
% Matlab PDF linear case agrees with the Simulink PDF case (Simulink PDF
% linear and nonlinear are identical). Looking at the results from P, PI,
% and PD controllers clearly PD is the best choice for this system when
% trying to control to the origin.

%% Functions
%
% <include>PhasePlane.m</include>
%
% <include>Prob2b.m</include>
%
% <include>Prob2c.m</include>
%
% <include>Prob2d.m</include>
%
% <include>Prob3c.m</include>
%
% <include>Prob3d.m</include>
%
% <include>Prob3e.m</include>


