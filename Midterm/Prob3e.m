function [dxdt] = Prob3e(t,x,A,B,C,D,K,Kd,r)

%declare global vars
global errorPast
global tPast
global FcnCall

%LINEAR MODEL
%until there is a previous time step, use proportional control
if(FcnCall<=1)
    %control input
    u = K*(r-C*x(1:4));
    %linearized system
    dxdt = A*x(1:4)+B*u;
end
if(FcnCall>=2)
    %PD control input--note that the derivative of error is calculated via
    %delta_e/delta_t
    u = K*(r-C*x(1:4))+Kd*((r-C*x(1:4))-errorPast(FcnCall-1))/(t-tPast(FcnCall-1));
    
    %linearized model
    dxdt = A*x(1:4)+B*u;
end

%increment number of times function has been called
FcnCall = FcnCall + 1;

%array of errors
errorPast(FcnCall) = r-C*x(1:4);

%time array
tPast(FcnCall) = t;

%NONLINEAR MODEL
%constants
M = 2;
m = 1;
g = 10;
L = 0.5;

%control input, same as linear case except now we can use x6 (aka x2) for
%the derivative of error term--note d/dt(e) = d/dt(r-C*x) = -C*x_dot = -x2
u_nonlin = K*(r-C*x(5:8))+Kd*(-x(6));

%nonlinear system
dxdt((5:8)) = [x(6);...
    -((3*m*cos(x(5))*sin(x(5))*x(6)^2)/4 + (3*u_nonlin*cos(x(5)))/(4*L) - (3*g*sin(x(5))*(M + m))/(4*L))/(M + m - (3*m*cos(x(5))^2)/4);...
    x(8);...
    (L*m*sin(x(5))*x(6)^2 + u_nonlin - (3*g*m*cos(x(5))*sin(x(5)))/4)/(M + m - (3*m*cos(x(5))^2)/4)];

% dxdt = dxdt.';

end

