function [dxdt] = Prob2c(t,x,A,B,C,D,L,x0_ss,u0_ss,mu,K)

%control input using state feedback
u = u0_ss-K*(x(1:2)-x0_ss);

%nonlinear system
dxdt(1:2) = [x(2);mu*(1-x(1)^2)*x(2)-x(1)+u];

%control input for extraction
dxdt(3) = u;
dxdt = dxdt.';


end

