function [dxdt] = Prob2b(t,x,A,B,C,D,L,x0_ss,u0_ss,mu)

%nonlinear system
dxdt(1:2) = [x(2);mu*(1-x(1)^2)*x(2)-x(1)+0.7];

%estimated states about linearized system
dxdt(3:4) = L*C*(x(1:2)-x0_ss)+(A-L*C)*(x(3:4)-x0_ss)+B*(0.7-u0_ss);
dxdt = dxdt.';

end

