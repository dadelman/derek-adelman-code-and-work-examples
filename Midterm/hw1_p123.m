close all; clear; clc

%% Problem 1
% a. Choose an appropriate state vector and derive the nonlinear state
% equation set:

% symbolic variables
syms t
syms p(t) T(t) u(t)
syms M m g L

% nonlinear derivative function
den = M + m - 3/4*m*cos(T)^2;
eq1 = diff(p,2) == (diff(T,1)^2*m*L*sin(T) - 3/4*m*g*sin(T)*cos(T) + u)/den;
eq2 = diff(T,2) == ( 3*g/(4*L)*(M+m)*sin(T) - 3*m/4*diff(T,1)^2*sin(T)*cos(T) - 3/(4*L)*u*cos(T) )/den;

% Reduce higher-order differential equations to a system of 1st-order equations
[eqs,vars] = reduceDifferentialOrder([eq1;eq2],[p;T]);

% permute equations to get the correct order
P = [2 4 1 3];
vars = vars(P);
eqs = eqs(P);

% obtain Mm(t,vars)*diff(vars)-Fm(t,vars)
[Mm,Fm] = massMatrixForm(eqs,vars);

% obtain the nonlinear derivative function
f = Mm\Fm

% obtain the nonlinear (but really linear) output function
go = p

% b. Linearize the model by assuming small angles and velocities, and
% derive the linear time-invariant state-space model (state and output
% equation)
% small angle approximations
f_small = subs(f,[sin(T) cos(T) vars(2)^2],[T 1 0]);

% substitute symbolic functions with symbolic variables
syms p_ T_ D2p_ D2T_ u_ real
vars_ = [T_;D2T_;p_;D2p_;u_];
f_small = subs(f_small,[vars;u],vars_);

% put into linear form
[AB_small,d_small] = equationsToMatrix(f_small,vars_)

% c. Show that the linear model can be divided into two models. One model
% describing the angle (theta-system) and one model describing the position
% (p-system) and show that the theta-system is completely decoupled from
% the p-system

% [no solution provided to the question]

%% Problem 2
% a. Linearize the inverted pendulum model from Problem 1 by using the
% method from Sect. 2.4 in Hendricks2008 and assume the following
% stationary state

% substitute the given parameter values
M0 = 2; m0 = 1; L0 = 0.5; g0 = 10;
f2 = subs(f,[M m L g],[M0 m0 L0 g0]);

% substitute symbolic functions with symbolic variables
syms p_ T_ D2p_ D2T_ u_ real
vars_ = [T_;D2T_;p_;D2p_;u_];
f2 = subs(f2,[vars;u],vars_);

% stationary point
X0 = [0 0 1 0]';
U0 = 0;

% compute Taylor expansion (i.e., linearization)
q = taylor(f2,vars_,'ExpansionPoint',[X0;U0],'Order',2);

% obtain linear matrices
[AB,d] = equationsToMatrix(q,vars_);
A_lin = AB(:,1:4)
B_lin = AB(:,5)
C_lin = [0 0 1 0]
D_lin = [0]

% b. Compare the result with the result from Problem 1b.
AB_small0 = subs(AB_small,[M m L g],[M0 m0 L0 g0])
A_small0 = AB(:,1:4)
B_small0 = AB(:,5)

% check if they are equal
isequal(A_small0,A_lin)
isequal(B_small0,B_lin)

%% Problem 3
% For each of these tasks, use the initial state values in Problem 2a, a
% constant driving force of 1 N, and final time of at least 20 s

% simulation options
TSPAN = [0 20];
OPTIONS = odeset('reltol',1e-13);
U = 1;

% initialize figures
hf = figure(1); hf.Color = 'w'; hold on
hf = figure(2); hf.Color = 'w'; hold on

% a. Run a dynamic simulation of original nonlinear model in Problem 1
odefun1 = odeFunction(f,vars,u,M,m,L,g);

% simulate nonlinear system
[T_f,Y_f] = ode45(@(t,x) odefun1(t,x,U,M0,m0,L0,g0),TSPAN,X0,OPTIONS);

% plot position and angle
figure(1)
plot(T_f,Y_f(:,[1 3]))

% plot velocities
figure(2)
plot(T_f,Y_f(:,[2 4])+X0([2 4])')

% b. Run a dynamic simulation of linearized model in Problem 1b
A = double(A_lin);
B = double(B_lin);

% simulate linear system
[T_lin,Y_lin] = ode45(@(t,x) A*x + B*U,TSPAN,X0-X0,OPTIONS);

% plot position and angle
figure(1)
plot(T_lin,Y_lin(:,[1 3])+X0([1 3])')
ylim([-15 15])
xlabel('t [s]')
legend('p (nonlinear)','T (nonlinear)','p (linear)','T (linear)')

% plot velocities
figure(2)
plot(T_lin,Y_lin(:,[2 4])+X0([2 4])')
ylim([-15 15])
xlabel('t [s]')
legend('Dp (nonlinear)','DT (nonlinear)','Dp (linear)','DT (linear)')

% c. Run a dynamic simulation of linearized model in Problem 2a. Is this a
% good approximation of the nonlinear system?

% Same simulation as part b

% It is a good approximation near the stationary operating point, but not
% far away