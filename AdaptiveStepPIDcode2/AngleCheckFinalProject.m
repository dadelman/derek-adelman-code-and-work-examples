function [] = AngleCheckFinalProject(y1,y2,y3)
flag = 0;
for v = 1:length(y1)
    if(y1(v,1) < -160*pi/180 || y1(v,1) > 160*pi/180)
        ErrorMessg = ['Warning: Calculated y1' ' value outside of joint range.\n'];
        fprintf(2,ErrorMessg)
        flag = 1;
    end
    if(y2(v,1) < -225*pi/180 || y2(v,1) > 45*pi/180)
        ErrorMessg = ['Warning: Calculated y2' ' value outside of joint range.\n'];
        fprintf(2,ErrorMessg)
        flag = 1;
    end
    if(y3(v,1) < -45*pi/180 || y3(v,1) > 225*pi/180)
        ErrorMessg = ['Warning: Calculated y3' ' value outside of joint range.\n'];
        fprintf(2,ErrorMessg)
        flag = 1;
    end  
    if(flag == 1)
        break;
    end
end
end