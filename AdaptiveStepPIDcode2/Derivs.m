function [dydt] = Derivs(t,y,w,varit,z,Gain,yi)
global IntErrorP
global IntErrorV
 %Called by myRK4.m currently config'd for MECH 564 final project
    %Rename trig terms
    c1 = cos(y(1));
    s1 = sin(y(1));
    c2 = cos(y(2));
    c22 = cos(y(2)+y(2));
    c3 = cos(y(3));
    s2 = sin(y(2));
    s3 = sin(y(3));
    c23 = cos(y(2)+y(3));
    s23 = sin(y(2)+y(3));
    
    %Calculate Inertia Matrix
    D(1,1) =  2.4574 + 1.7181*c2*c2 + 0.4430*s23*s23 - 0.0324*c2*c23 - 0.0415*c23*s23 + 0.9378*c2*s23;
    D(1,2) = 2.2312*s2 - 0.0068*s23 - 0.1634*c23;
    D(1,3) = -0.0068*s23 - 0.1634*c23;
    D(2,1) = 2.2312*s2 - 0.0068*s23 - 0.1634*c23;
    D(2,2) = 5.1285 + 0.9378*s3 - 0.0324*c3;
    D(2,3) = 0.4424 + 0.4689*s3 - 0.0162*c3;
    D(3,1) = -0.0068*s23 - 0.1634*c23;
    D(3,2) = 0.4424 + 0.4689*s3 - 0.0162*c3;
    D(3,3) = 1.0236;
    
    %Calculate Christoffel symbols
    C(1,1,1) = 0;
    C(1,2,1) = 0.0207 - 1.2752*c2*s2 + 0.4429*c3*s3 - 0.8859*s2*s3*s23 + 0.0325*c2*s23 + 0.4689*c2*c23 - 0.4689*s2*s23 - 0.0461*c22 - 0.0415*c23*c23 - 0.0163*s3;
    C(1,3,1) = 0.0207 + 0.4429*c2*s2 + 0.4429*c3*s3 - 0.8859*s2*s3*s23 + 0.0163*c2*s23 + 0.4689*c2*c23 - 0.0415*c23*c23;
    C(2,1,1) = 0.0207 - 1.2752*c2*s2 + 0.4429*c3*s3 - 0.8859*s2*s3*s23 + 0.0325*c2*s23 + 0.4689*c2*c23 - 0.4689*s2*s23 - 0.0461*c22 - 0.0415*c23*c23 - 0.0163*s3;
    C(2,2,1) = 1.8181*c2 + 0.1634*s23 - 0.0068*c23;
    C(2,3,1) = 0.1634*s23 - 0.0068*c23;
    C(3,1,1) = 0.0207 + 0.4429*c2*s2 + 0.4429*c3*s3 - 0.8859*s2*s3*s23 + 0.0163*c2*s23 + 0.4689*c2*c23 - 0.0415*c23*c23;
    C(3,2,1) = 0.1634*s23 - 0.0068*c23;
    C(3,3,1) = 0.1634*s23 - 0.0068*c23;
    C(1,1,2) = -(0.0207 - 1.2752*c2*s2 + 0.4429*c3*s3 - 0.8859*s2*s3*s23 + 0.0325*c2*s23 + 0.4689*c2*c23 - 0.4689*s2*s23 - 0.0461*c22 - 0.0415*c23*c23 - 0.0163*s3);
    C(1,2,2) = 0;
    C(1,3,2) = 0;
    C(2,1,2) = 0;
    C(2,2,2) = 0;
    C(2,3,2) = 0.4689*c3 + 0.0162*s3;
    C(3,1,2) = 0;
    C(3,2,2) = 0.4689*c3 + 0.0162*s3;
    C(3,3,2) = 0.4689*c3 + 0.0162*s3;
    C(1,1,3) = -(0.0207 + 0.4429*c2*s2 + 0.4429*c3*s3 - 0.8859*s2*s3*s23 + 0.0163*c2*s23 + 0.4689*c2*c23 - 0.0415*c23*c23);
    C(1,2,3) = 0;
    C(1,3,3) = 0;
    C(2,1,3) = 0;
    C(2,2,3) = -(0.4689*c3 + 0.0162*s3);
    C(2,3,3) = 0;
    C(3,1,3) = 0;
    C(3,2,3) = 0;
    C(3,3,3) = 0;
    
    %Restructure christoffel sym for diff equns
    Csym = zeros(3,3);
    for k = 1:3
        for j = 1:3
            for i = 1:3
                Csym(k,j) = Csym(k,j) + C(i,j,k)*y(i+3);
            end
        end
    end
    
    %gravity terms
    g(1,1) = 0;
    g(2,1) = -48.5564*c2 + 1.0462*s2 + 0.3683*c23 - 10.6528*s23;
    g(3,1) =  0.3683*c23 - 10.6528*s23;
    
%Rename vars for Jdot
    Th1 = y(1);
    Th2 = y(2);
    Th3 = y(3);
    Th1d = y(4);
    Th2d = y(5);
    Th3d = y(6);
    
%Constants
    a1 = 0;
    a2 = 431.8; %mm
    a3 = -020.32;
    d1 = 0;
    d2 = 149.09;
    d3 = 0;
    d4 = 433.07;
    
%Calculate the Jacobian (Partial diff done in separate script)
    J = [- d2*cos(Th1) - d4*sin(Th2 + Th3)*sin(Th1) - a2*cos(Th2)*sin(Th1) - a3*cos(Th2 + Th3)*sin(Th1), d4*cos(Th2 + Th3)*cos(Th1) - a2*cos(Th1)*sin(Th2) - a3*sin(Th2 + Th3)*cos(Th1), d4*cos(Th2 + Th3)*cos(Th1) - a3*sin(Th2 + Th3)*cos(Th1);
        a2*cos(Th1)*cos(Th2) - d2*sin(Th1) + a3*cos(Th2 + Th3)*cos(Th1) + d4*sin(Th2 + Th3)*cos(Th1), d4*cos(Th2 + Th3)*sin(Th1) - a2*sin(Th1)*sin(Th2) - a3*sin(Th2 + Th3)*sin(Th1), d4*cos(Th2 + Th3)*sin(Th1) - a3*sin(Th2 + Th3)*sin(Th1);
          0, - a3*cos(Th2 + Th3) - d4*sin(Th2 + Th3) - a2*cos(Th2), - a3*cos(Th2 + Th3) - d4*sin(Th2 + Th3)];

%Calculate time derivative of Jacobian (done in separate script)
    Jdot = [Th1d*d2*sin(Th1) + a3*sin(Th2 + Th3)*sin(Th1)*(Th2d + Th3d) - Th1d*a3*cos(Th2 + Th3)*cos(Th1) - Th1d*d4*sin(Th2 + Th3)*cos(Th1) - Th1d*a2*cos(Th1)*cos(Th2) + Th2d*a2*sin(Th1)*sin(Th2) - d4*cos(Th2 + Th3)*sin(Th1)*(Th2d + Th3d), ...
            Th1d*a3*sin(Th2 + Th3)*sin(Th1) - Th1d*d4*cos(Th2 + Th3)*sin(Th1) - Th2d*a2*cos(Th1)*cos(Th2) + Th1d*a2*sin(Th1)*sin(Th2) - a3*cos(Th2 + Th3)*cos(Th1)*(Th2d + Th3d) - d4*sin(Th2 + Th3)*cos(Th1)*(Th2d + Th3d),...
            Th1d*a3*sin(Th2 + Th3)*sin(Th1) - Th1d*d4*cos(Th2 + Th3)*sin(Th1) - a3*cos(Th2 + Th3)*cos(Th1)*(Th2d + Th3d) - d4*sin(Th2 + Th3)*cos(Th1)*(Th2d + Th3d);
            d4*cos(Th2 + Th3)*cos(Th1)*(Th2d + Th3d) - Th1d*a3*cos(Th2 + Th3)*sin(Th1) - Th1d*d4*sin(Th2 + Th3)*sin(Th1) - Th1d*a2*cos(Th2)*sin(Th1) - Th2d*a2*cos(Th1)*sin(Th2) - Th1d*d2*cos(Th1) - a3*sin(Th2 + Th3)*cos(Th1)*(Th2d + Th3d),...
            Th1d*d4*cos(Th2 + Th3)*cos(Th1) - d4*sin(Th2 + Th3)*sin(Th1)*(Th2d + Th3d) - Th1d*a3*sin(Th2 + Th3)*cos(Th1) - Th1d*a2*cos(Th1)*sin(Th2) - Th2d*a2*cos(Th2)*sin(Th1) - a3*cos(Th2 + Th3)*sin(Th1)*(Th2d + Th3d),...
            Th1d*d4*cos(Th2 + Th3)*cos(Th1) - d4*sin(Th2 + Th3)*sin(Th1)*(Th2d + Th3d) - Th1d*a3*sin(Th2 + Th3)*cos(Th1) - a3*cos(Th2 + Th3)*sin(Th1)*(Th2d + Th3d);
            0, a3*sin(Th2 + Th3)*(Th2d + Th3d) - d4*cos(Th2 + Th3)*(Th2d + Th3d) + Th2d*a2*sin(Th2), a3*sin(Th2 + Th3)*(Th2d + Th3d) - d4*cos(Th2 + Th3)*(Th2d + Th3d)];     

%Set nonlinear controller input
%     w = pi/4;
    R = 250;
    Yd = [-0.866*R*cos(w*t)-560;
          R*sin(w*t);
          0.5*R*cos(w*t)-80];
    Yd_d = [(433*R*w*sin(t*w))/500
            R*w*cos(t*w)
            -(R*w*sin(t*w))/2];
    Yd_dd = [(433*R*w^2*cos(t*w))/500
             -R*w^2*sin(t*w)
             -(R*w^2*cos(t*w))/2];

    
%Calculate h (hand position) and time derivative of h for input to controller
    h = [a3.*c1.*c23 + d4.*c1.*s23 + a2.*c1.*c2 - d2.*s1;
         a3.*s1.*c23 + d4.*s1.*s23 + a2.*s1.*c2 + d2.*c1;
         -a3.*s23 + d4.*c23 - a2.*s2];
     
    hdot = [d4*cos(Th1)*cos(Th2 + Th3)*(Th2d + Th3d) - Th1d*d2*cos(Th1) - a3*cos(Th1)*sin(Th2 + Th3)*(Th2d + Th3d) - Th1d*a2*cos(Th2)*sin(Th1) - Th2d*a2*cos(Th1)*sin(Th2) - Th1d*a3*sin(Th1)*cos(Th2 + Th3) - Th1d*d4*sin(Th1)*sin(Th2 + Th3);
            d4*sin(Th1)*cos(Th2 + Th3)*(Th2d + Th3d) - Th1d*d2*sin(Th1) - a3*sin(Th1)*sin(Th2 + Th3)*(Th2d + Th3d) + Th1d*a2*cos(Th1)*cos(Th2) - Th2d*a2*sin(Th1)*sin(Th2) + Th1d*a3*cos(Th1)*cos(Th2 + Th3) + Th1d*d4*cos(Th1)*sin(Th2 + Th3);
            - a3*cos(Th2 + Th3)*(Th2d + Th3d) - d4*sin(Th2 + Th3)*(Th2d + Th3d) - Th2d*a2*cos(Th2)];

%Calculate input to linear controller and nonlinear feedback controller value

    if(Gain == 'NA')
        tao = [1 0 0]';
    elseif(Gain == 1)
        u = [1 0 0]';
        tao = D*(J^-1)*(u-Jdot*y(4:6))+Csym*y(4:6)+g;
    elseif(Gain == 2)
        u = [0 1 0]';
        tao = D*(J^-1)*(u-Jdot*y(4:6))+Csym*y(4:6)+g;
    elseif(Gain == 3)
        u = [0 0 1]';
        tao = D*(J^-1)*(u-Jdot*y(4:6))+Csym*y(4:6)+g;
    else
        Kd = Gain(1); %Set proportional and derivative state-space gains
        Kp = Gain(2);
        Ki = Gain(3);
        IntErrorP = IntErrorP + (Yd-h);
        IntErrorV = IntErrorV + (Yd_d-J*y(4:6));
        Error = IntErrorP + IntErrorV;
        u = Yd_dd + Kd*(Yd_d-J*y(4:6)) + Kp*(Yd-h) + Ki*Error;
        tao = D*(J^-1)*(u-Jdot*y(4:6))+Csym*y(4:6)+g;
    end

%Differential equations (2nd order system of 3 equns reduce to 1st order of
%6 equns)
    D = D*varit(z);
    Csym = Csym*varit(z);
    g = g*varit(z);
    dydt = [y(4);y(5);y(6); -(D^-1)*Csym*y(4:6) - (D^-1)*g + (D^-1)*tao];

end