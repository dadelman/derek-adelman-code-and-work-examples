%%
%%
clear
clc
format compact
close all
%constants of robot
a1 = 0;
a2 = 431.8; %mm
a3 = -020.32;
d1 = 0;
d2 = 149.09;
d3 = 0;
d4 = 433.07;
global IntErrorP
global IntErrorV
IntErrorP = 0;
IntErrorV = 0;
%set intial and final time
time0 = 0
timeF = 25
StepSize = .001;

fprintf('Computing Task 4.2.1 ...\n')

Gain = 'NA';
z = 1;
w = pi/4;
varit = [1 1.05 1.1 1.3]';
y = [0 0 0 0 0 0]'; %init start config
%%
[y1,y2,y3,y4,y5,y6,Yd,Yd_d,Yd_dd,T,H,E,ET] = myRK4(y,z,w,varit,Gain,time0,timeF,StepSize);

%Check angles are withing range
AngleCheckFinalProject(y1,y2,y3)

%plot angle of each joint
figure
hold on
plot(T,y1)
plot(T,y2)
plot(T,y3)
xlabel('time (sec)')
ylabel('angle (rads)')
title('Angular Displacement by Joint')
legend('Joint 1', 'Joint 2', 'Joint 3')

%Position of wrist center
c1 = cos(y1);
s1 = sin(y1);
c2 = cos(y2);
c22 = cos(y2+y2);
c3 = cos(y3);
s2 = sin(y2);
s3 = sin(y3);
c23 = cos(y2+y3);
s23 = sin(y2+y3);

%position of wrist column 4 of homo. trans matrix
p = [a3.*c1.*c23 + d4.*c1.*s23 + a2.*c1.*c2 - d2.*s1,...
 a3.*s1.*c23 + d4.*s1.*s23 + a2.*s1.*c2 + d2.*c1,...
 -a3.*s23 + d4.*c23 - a2.*s2];

%plot wrist center in terms of x,y,z
figure
hold on
plot(T,p)
xlabel('time (sec)')
ylabel('Position (mm)')
title('X,Y,Z of Wrist Center from Initial Config')
legend('x','y','z')

fprintf('Completed\n')

figure
plot3(p(:,1), p(:,2),p(:,3))
xlabel('x (mm)')
ylabel('y (mm)')
zlabel('z (mm)')
title('3D plot of Dynamics')

figure
plot(T(1:end-1),H)
title('StepSize')
figure
plot(T(1:end-1),E)
title('Rel Error')
figure
plot(T(1:end-1),ET)
title('errorTol')

%%
fprintf('Computing Task 4.2.2 ...\n')

y = [0 0 0 0 0 0]'; %re-init config var

for r = 1:3
    Gain = r;
    time = time0; %init time var
    IntErrorP = 0;
    IntErrorV = 0;
    [y1,y2,y3,y4,y5,y6,Yd,Yd_d,Yd_dd,T,H,E,ET] = myRK4(y,z,w,varit,Gain,time0,timeF,StepSize);

    %Check angles are within range
    AngleCheckFinalProject(y1,y2,y3)

    %Position of wrist center
    c1 = cos(y1);
    s1 = sin(y1);
    c2 = cos(y2);
    c22 = cos(y2+y2);
    c3 = cos(y3);
    s2 = sin(y2);
    s3 = sin(y3);
    c23 = cos(y2+y3);
    s23 = sin(y2+y3);

    p = [a3.*c1.*c23 + d4.*c1.*s23 + a2.*c1.*c2 - d2.*s1,...
     a3.*s1.*c23 + d4.*s1.*s23 + a2.*s1.*c2 + d2.*c1,...
     -a3.*s23 + d4.*c23 - a2.*s2];

    u{1,1} = '1 0 0';
    u{2,1} = '0 1 0';
    u{3,1} = '0 0 1';

    figure
    hold on
    plot(T,p)
    xlabel('time (sec)')
    ylabel('Position (mm)')
    title(['X,Y,Z of Wrist Center from Initial Config u = ' u{r}])
    legend('x','y','z')

end
figure
plot(T(1:end-1),H)
title('StepSize')
figure
plot(T(1:end-1),E)
title('Rel Error--mean(e)')
figure
plot(T(1:end-1),ET)
title('errorTol')
fprintf('Completed\n')
%%
fprintf('Computing Task 4.2.3 & 4.2.4...\n\n')

%Cases 1-4
Y{1,1} = [-776.5 0 45]'; %mm
Y{2,1} = [-500 -100 0]'; %mm
Y{3,1} = [-776.5 0 45]'; %mm
Y{4,1} = [-500 -100 0]'; %mm

for o = 1:length(Y)
    for z = 1:length(varit)

        clear y1 y2 y3 y5 y5 y6 Yd Yd_d Yd_dd %clear vars from previous cases
        IntErrorP = 0;
        IntErrorV = 0;

    %set frequency and Gain values Kd and Kp and Ki
        if(o == 1)
            w = pi/4;
            Gain = [90 30 6]';
        end
        if(o == 2)
            w = pi/4;
            Gain = [5 5 1]';
        end
        if(o == 3)
            w = pi/2;
            Gain = [5 5 1]';
        end
        if(o == 4)
            w = pi/2;
            Gain = [250 30 6]';
        end

        disp(['Initial joint config for case ' num2str(o) ', DCg variation ' num2str((varit(z)-1)*100) '%'...
            ', Kd=' num2str(Gain(1)) ' Kp=' num2str(Gain(2)) ' Ki=' num2str(Gain(3)) ', w=' num2str(w) ' rad/s'])

    %Find Th1,Th2,Th3 given Y(0)--Method used Ziegler and Lee, Inver. Kine. Puma
    %robots 1983--see derivations soln used is LEFT ARM and BELOW ARM
        h = (Y{o,1}(1)^2+Y{o,1}(2)^2)^0.5;
        A = (h^2-d2^2)^0.5;
        Calpha = A/h;
        Salpha = d2/h;
        Cbeta = Y{o,1}(1)/h;
        Sbeta = Y{o,1}(2)/h;
        stheta1 = Sbeta*Calpha-Salpha*Cbeta;
        ctheta1 = Cbeta*Calpha+Sbeta*Salpha;
        y1(1,1) = atan2(stheta1,ctheta1) %joint 1

        K = (A^2+Y{o,1}(3)^2)^0.5;
        Calpha = A/K;
        Salpha = -Y{o,1}(3)/K;
        Cbeta = ((a3^2+d4^2)-a2^2-K^2)/(-2*a2*K);
        Sbeta = sqrt(1-Cbeta^2);
        stheta2 = Sbeta*Calpha+Salpha*Cbeta;
        ctheta2 = Cbeta*Calpha-Sbeta*Salpha;
        y2(1,1) = atan2(stheta2,ctheta2) %joint 2

        Cbeta = abs(a3)/(a3^2+d4^2)^0.5;
        Sbeta = d4/(a3^2+d4^2)^0.5;
        Cphi = (-K^2+a2^2+a3^2+d4^2)/(2*a2*(a3^2+d4^2)^0.5);
        Sphi = (1-Cphi^2)^0.5;
        stheta3 = Sphi*Cbeta-Cphi*Sbeta;
        ctheta3 = Cphi*Cbeta+Sphi*Sbeta;
        y3(1,1) = atan2(stheta3,ctheta3) %joint 3

        y = [y1 y2 y3 0 0 0]'; %init pos/vel vars for start config

        [y1,y2,y3,y4,y5,y6,Yd,Yd_d,Yd_dd,T,H,E,ET] = myRK4(y,z,w,varit,Gain,time0,timeF,StepSize);

    %Check angles are withing range
        AngleCheckFinalProject(y1,y2,y3)

    %Position of wrist center
        c1 = cos(y1);
        s1 = sin(y1);
        c2 = cos(y2);
        c22 = cos(y2+y2);
        c3 = cos(y3);
        s2 = sin(y2);
        s3 = sin(y3);
        c23 = cos(y2+y3);
        s23 = sin(y2+y3);

        p = [a3.*c1.*c23 + d4.*c1.*s23 + a2.*c1.*c2 - d2.*s1,...
             a3.*s1.*c23 + d4.*s1.*s23 + a2.*s1.*c2 + d2.*c1,...
             -a3.*s23 + d4.*c23 - a2.*s2];

        Th1 = y1;
        Th2 = y2;
        Th3 = y3;
        Th1d = y4;
        Th2d = y5;
        Th3d = y6;

        pdot = [d4.*cos(Th1).*cos(Th2 + Th3).*(Th2d + Th3d) - Th1d.*d2.*cos(Th1) - a3.*cos(Th1).*sin(Th2 + Th3).*(Th2d + Th3d) - Th1d.*a2.*cos(Th2).*sin(Th1) - Th2d.*a2.*cos(Th1).*sin(Th2) - Th1d.*a3.*sin(Th1).*cos(Th2 + Th3) - Th1d.*d4.*sin(Th1).*sin(Th2 + Th3),...
            d4.*sin(Th1).*cos(Th2 + Th3).*(Th2d + Th3d) - Th1d.*d2.*sin(Th1) - a3.*sin(Th1).*sin(Th2 + Th3).*(Th2d + Th3d) + Th1d.*a2.*cos(Th1).*cos(Th2) - Th2d.*a2.*sin(Th1).*sin(Th2) + Th1d.*a3.*cos(Th1).*cos(Th2 + Th3) + Th1d.*d4.*cos(Th1).*sin(Th2 + Th3),...
            - a3.*cos(Th2 + Th3).*(Th2d + Th3d) - d4.*sin(Th2 + Th3).*(Th2d + Th3d) - Th2d.*a2.*cos(Th2)];

    %plot position error Yd-Y
        figure
        plot(T,Yd(:,1:3)-p(:,1:3));
        legend('x error', 'y error', 'z error')
        title(['Error for Case ' num2str(o) ': DCg variation ' num2str((varit(z)-1)*100) '%'...
            ', Kd=' num2str(Gain(1)) ' Kp=' num2str(Gain(2)) ' Ki=' num2str(Gain(3)) ', w=' num2str(w) ' rad/s'])
        xlabel('time (sec)')
        ylabel('Error (mm)')

    %plot velocity error Yd_d-pdot
        figure
        plot(T,Yd_d(:,1:3)-pdot(:,1:3));
        legend('x vel error', 'y vel error', 'z vel error')
        title(['Velocity Error for Case ' num2str(o) ': DCg variation ' num2str((varit(z)-1)*100) '%'...
            ', Kd=' num2str(Gain(1)) ' Kp=' num2str(Gain(2)) ' Ki=' num2str(Gain(3)) ', w=' num2str(w) ' rad/s'])
        xlabel('time (sec)')
        ylabel('Velocity Error (mm/s)')

    %plot 3d trace of input/robot
        figure
        hold on
        plot3(Yd(:,1),Yd(:,2),Yd(:,3),p(:,1),p(:,2),p(:,3))
        v = [-0.3 -0.7 0.45];
        view(v)
        legend('Input Trajectory','Robot Trace')
        xlabel('x (mm)')
        ylabel('y (mm)')
        zlabel('z (mm)')
        title(['Robot Tracking of Case ' num2str(o) ': DCg variation ' num2str((varit(z)-1)*100) '%'...
            ', Kd=' num2str(Gain(1)) ' Kp=' num2str(Gain(2)) ' Ki=' num2str(Gain(3)) ', w=' num2str(w) ' rad/s'])

    end
end

fprintf('Task 4.2.3 & 4.2.4 Completed\n')
disp('END SIMULATION')

%% Functions
%% 
% <include>Derivs.m</include>
% 
% <include>myRK4.m</include>
% 
% <include>AngleCheckFinalProject.m</include>
