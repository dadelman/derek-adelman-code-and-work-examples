function [y1,y2,y3,y4,y5,y6,Yd,Yd_d,Yd_dd,T,H,E,ET] = myRK4(n,z,w,varit,Gain,time0,timeF,StepSize)
%MYRK4 Numerical Runge Kutta method 4th order currently configured for
%MECH564 final project works in conjunction with Derivs.m
t = 1;
time = time0;
x = 1;
upperTol = 0.05;
lowerTol = 1e-3;
errorTol = 1e-3;
errorTolMax = 1.25e-2;

while x == 1
    flag = 0;
%         while x == 1
            
            %init pos/angular vel vars
            y1(1,1) = n(1);
            y2(1,1) = n(2);
            y3(1,1) = n(3);
            y4(1,1) = n(4);
            y5(1,1) = n(5);
            y6(1,1) = n(6);

            %place in vector
            y = [y1(t,1) y2(t,1) y3(t,1) y4(t,1) y5(t,1) y6(t,1)]';
            if(t>1)
            yi = [y1(t-1,1) y2(t-1,1) y3(t-1,1)]';
            else
                yi = 0;
            end

     %/////////////////////////////////////////////////////////////////////////   
     %Perform 4th order Runge-Kutta method (see Numerical Methods for Eng. 7th
     %ed Chapra & Canale) [2]
    %adapt step size step size
    
        
               %beginnnig of step slope approx
                    k1 = Derivs(time,y,w,varit,z,Gain,yi);
                    ymidpoint = y + k1.*StepSize/5;
                    
                %mid of step slope approx
                    k2 = Derivs(time+StepSize/5,ymidpoint,w,varit,z,Gain,yi);
                    ymid2 = y + k1.*StepSize*3/40 + k2.*StepSize*9/40;
                
                %2nd mid of step slope approx
                    k3 = Derivs(time+StepSize*3/10,ymid2,w,varit,z,Gain,yi);
                    ymid3 = y + 3/10*k1.*StepSize - 9/10*k2.*StepSize + 6/5*k3.*StepSize;
               
                %mid of step slope approx
                    k4 = Derivs(time+StepSize*3/5,ymid3,w,varit,z,Gain,yi);
                    ymid4 = y + -11/54*k1.*StepSize +5/2*k2.*StepSize + -70/27*k3.*StepSize +35/27*k4.*StepSize;
                    
                %mid of slope k5
                    k5 = Derivs(time+StepSize,ymid4,w,varit,z,Gain,yi);
                    yEnd = y + 1631/55296*k1.*StepSize + 175/512*k2.*StepSize +575/13824*k3.*StepSize +44275/110592*k4.*StepSize +253/4096*k5.*StepSize;
                    
                %End of approx
                    k6 = Derivs(time+StepSize*7/8,yEnd,w,varit,z,Gain,yi);
                    
    %Calculate next value for each dependent var 4th order
            v1(t+1,1) = y1(t,1) + (37/378*k1(1,1) + 250/621*k3(1,1) + 125/594*k4(1,1) +512/1771*k6(1,1))*StepSize;
            v2(t+1,1) = y2(t,1) + (37/378*k1(2,1) + 250/621*k3(2,1) + 125/594*k4(2,1) +512/1771*k6(2,1))*StepSize;
            v3(t+1,1) = y3(t,1) + (37/378*k1(3,1) + 250/621*k3(3,1) + 125/594*k4(3,1) +512/1771*k6(3,1))*StepSize;
            v4(t+1,1) = y4(t,1) + (37/378*k1(4,1) + 250/621*k3(4,1) + 125/594*k4(4,1) +512/1771*k6(4,1))*StepSize;
            v5(t+1,1) = y5(t,1) + (37/378*k1(5,1) + 250/621*k3(5,1) + 125/594*k4(5,1) +512/1771*k6(5,1))*StepSize;
            v6(t+1,1) = y6(t,1) + (37/378*k1(6,1) + 250/621*k3(6,1) + 125/594*k4(6,1) +512/1771*k6(6,1))*StepSize;
            
     %Calculate next value for each dependent var 5th order
            y1(t+1,1) = y1(t,1) + (2825/27648*k1(1,1) + 18575/48384*k3(1,1) + 13525/55296*k4(1,1) +277/14336*k5(1,1) + 1/4*k6(1,1))*StepSize;
            y2(t+1,1) = y2(t,1) + (2825/27648*k1(2,1) + 18575/48384*k3(2,1) + 13525/55296*k4(2,1) +277/14336*k5(2,1) + 1/4*k6(2,1))*StepSize;
            y3(t+1,1) = y3(t,1) + (2825/27648*k1(3,1) + 18575/48384*k3(3,1) + 13525/55296*k4(3,1) +277/14336*k5(3,1) + 1/4*k6(3,1))*StepSize;
            y4(t+1,1) = y4(t,1) + (2825/27648*k1(4,1) + 18575/48384*k3(4,1) + 13525/55296*k4(4,1) +277/14336*k5(4,1) + 1/4*k6(4,1))*StepSize;
            y5(t+1,1) = y5(t,1) + (2825/27648*k1(5,1) + 18575/48384*k3(5,1) + 13525/55296*k4(5,1) +277/14336*k5(5,1) + 1/4*k6(5,1))*StepSize;
            y6(t+1,1) = y6(t,1) + (2825/27648*k1(6,1) + 18575/48384*k3(6,1) + 13525/55296*k4(6,1) +277/14336*k5(6,1) + 1/4*k6(6,1))*StepSize;
%             Y = [y1(t+1,1) y2(t+1,1)  y3(t+1,1)  y4(t+1,1)  y5(t+1,1) y6(t+1,1)]';
            e = abs([y1(t+1,1)-v1(t,1) y2(t+1,1)-v2(t,1) y3(t+1,1)-v3(t,1) y4(t+1,1)-v4(t,1) y5(t+1,1)-v5(t,1) y6(t+1,1)-v6(t,1)])';
            E(t,1) = max(e);
            H(t,1) = StepSize;
            ET(t,1) = errorTol;
            s = (errorTol*StepSize/(2*max(e)))^(1/4);
%             errorTol = errorTolMax*abs(mean(y))
            if(max(e)<=errorTolMax)
                a = 0.2;
            else
                a = 0.25;
            end
            
%             StepSize = StepSize*abs(errorTol/mean(e))^a;
            
            
            %errorTolMax is the maximum the error can get. if above this,
            %recalculate. errorTol can float rising or lowering to 
%             if(mean(e)>=errorTol)
%                 if(StepSize<lowerTol)
% %                     disp('Cannot achieve error tolerance with minimum StepSize')
%                     errorTol = errorTol + 1e-4;
%                     flag = 1;
%                     break
%                 end
%                 StepSize = s*StepSize;
%             end
%             if(mean(e)<errorTol)  
% %                 errorTol = errorTol - 1e-4;
%                 break
%             end
            
            
%         end 
%END RUNGE KUTTA///////////////////////////////////////////////////////////////////////////////////
%Desired Position
    % w = pi/4;
            R = 250; %mm
            Yd(t,1:3) = [-0.866*R*cos(w*time)-560;
                 R*sin(w*time);
                 0.5*R*cos(w*time)-80]';
            Yd_d(t,1:3) = [w*0.866*R*sin(w*time);
                w*R*cos(w*time);
                -w*.5*R*sin(w*time)]';
            Yd_dd(t,1:3) = [w^2*0.866*R*sin(w*time);
                -w^2*R*sin(w*time);
                -w^2*0.5*R*cos(w*time)]';
            
    %time/increment counters and final T Yx_xx values     
            T(t,1) = time;
            time = time + StepSize;
            StepSize;
            %adjust step size
            StepSize = StepSize*abs(errorTolMax/max(e))^a;
%             if(flag == 1)
%             StepSize = lowerTol;
%             elseif(flag == 0)
%             StepSize = StepSize/s; 
%             end
            
            %
            T(t+1,1) = time;
            Yd(t+1,1:3) = [-0.866*R*cos(w*time)-560;
                 R*sin(w*time);
                 0.5*R*cos(w*time)-80]';
            Yd_d(t+1,1:3) = [w*0.866*R*sin(w*time);
                w*R*cos(w*time);
                -w*.5*R*sin(w*time)]';
            Yd_dd(t+1,1:3) = [w^2*0.866*R*sin(w*time);
                -w^2*R*sin(w*time);
                -w^2*0.5*R*cos(w*time)]';
            
            t = t + 1;
    %exit condition
            if(time>=timeF)
                break;
            end
end
end